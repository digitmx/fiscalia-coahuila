## Synopsis

Tema de Wordpress para Fiscalía Coahuila

## Code Example

La página web esta en:

* 

## Motivation

El tema servirá para mostrar los contenidos de Fiscalía Abierta

## Installation

El Tema es para Wordpress, solo se tiene que copiar el contenido de la carpeta "dist" a la carpeta "themes" dentro de "wp-content". Para generar la carpeta "dist" necesitas ejecutar "grunt" desde la terminal. El tema esta basado en Material Design con MaterializeCSS, además de usar GruntJS para minificación de CSS, JS e imágenes.

## Contributors

* 

## License

Copyright 2018 - Fiscalía Coahuila

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.