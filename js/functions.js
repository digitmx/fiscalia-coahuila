
	$( document ).ready(function() {
		
		//Read Values
		var base_url = $('#base_url').val();
		var theme_url = $('#theme_url').val();
    
    	//backend-login click
    	$('#backend-login').on('click', function(e) {
	    	e.preventDefault();
	    	
	    	window.location.href = base_url + "/backend";
	    	
	    	return false;
    	});
    	
    	$('#backend-dashboard').on('click', function(e) {
	    	e.preventDefault();
	    	
	    	window.location.href = base_url + "/backend/dashboard";
	    	
	    	return false;
    	});
    	
    	$('#index').on('click', function(e) {
	    	e.preventDefault();
	    	
	    	window.location.href = base_url;
	    	
	    	return false;
    	});
    	
    	$('#home-rol1').on('click', function(e) {
	    	e.preventDefault();
	    	
	    	window.location.href = base_url + "/rol-1";
	    	
	    	return false;
    	});
    	
    	$('#home-rol2').on('click', function(e) {
	    	e.preventDefault();
	    	
	    	window.location.href = base_url + "/rol-2";
	    	
	    	return false;
    	});
    	
    	$('#home-rol3').on('click', function(e) {
	    	e.preventDefault();
	    	
	    	window.location.href = base_url + "/rol-3";
	    	
	    	return false;
    	});
    	
    	$('#home-rol4').on('click', function(e) {
	    	e.preventDefault();
	    	
	    	window.location.href = base_url + "/rol-4";
	    	
	    	return false;
    	});
    	
    	$('.logOut').on('click', function(e) {
	    	e.preventDefault();
	    	
	    	window.location.href = base_url + "/logout/";
	    	
	    	return false;
    	});
    	
    	$('#register1_form #btnCancel').on('click', function(e) {
	    	e.preventDefault();
	    	var rol = $('#rol').val();
	    	window.location.href = base_url + '/' + rol + '/';
	    	
	    	return false;
    	});
    	
    	$('#edit_form #btnCancel').on('click', function(e) {
	    	e.preventDefault();
	    	var rol = $('#rol').val();
	    	window.location.href = base_url + '/' + rol + '/';
	    	
	    	return false;
    	});
    	
		$('.btnMore').on('click', function () {
			var text = $.trim($(this).text());
			var rel = $.trim($(this).attr('rel'));
			if(text === "Ver Mas"){
				$(this).html('Ver Menos <img src="'+ theme_url +'/img/icono__btnvermas.svg" class="rotateMenos">');
			} else{
				$(this).html('Ver Mas <img src="'+ theme_url +'/img/icono__btnvermas.svg">');
			}
		});
		
		$('.dropdown-toggle').dropdown();
		
		//Modal Notificacion
		
		$("#enviar-not").click(function(){
			$(".footer-notificacion, .body-notificacion").hide();
			$(".body-notificacion-aceptar, .footer-notificacion-aceptar").show();
		});
		
		$("#aceptar-not").click(function(){
			$(".body-notificacion-aceptar, .footer-notificacion-aceptar").hide();
			$(".footer-notificacion, .body-notificacion").show();
		});
		
		$("#guardar-apr").click(function(){
			$(".footer-aprobar, .body-aprobar").hide();
			$(".body-aprobar-aceptar, .footer-aprobar-aceptar").show();
		});
		
		$("#aceptar-apr").click(function(){
			$(".body-aprobar-aceptar, .footer-aprobar-aceptar").hide();
			$(".footer-aprobar, .body-aprobar").show();
		});
		
		//$("#guardar-crear").click(function(){
		//	$(".footer-notificacion, .body-notificacion").hide();
		//	$(".body-notificacion-aceptar, .footer-notificacion-aceptar").show();
		//});
		
		//$("#aceptar-crear").click(function(){
		//	$(".body-notificacion-aceptar, .footer-notificacion-aceptar").hide();
		//	$(".footer-notificacion, .body-notificacion").show();
		//});
		
		//Validation Login
		
		$("#login_form").validate({
		   rules: {
		       inputEmail: {
			   		required: true,
			   		email: true
		       },
		       inputPassword: {
		        	required: true
		       }
		   	}
		  });
		  
		  // #login_form #btnSubmit click
		  $('#login_form #btnSubmit').on('click', function(e) {
		   	e.preventDefault();
		   
		   	if ($('#login_form').valid())
		   	{
		    	//Leemos los Valores
				var email = $('#inputEmail').val();
				var password = $('#inputPassword').val();
				var param = '{"msg": "doLogin","fields": {"email": "' + email + '","password": "' + password + '"}}';
		    
				//Verificamos el Correo Electrónico
				$('#login_form #btnSubmit').prop( 'disabled', true );
				$('#login_form #btnSubmit').val('PROCESANDO...');
		   
				//Request API
				$.post(base_url + '/api', { param: param }).done(function( data ) {
					console.log(data);
					
					//Check Result
					if (data.status == 1)
					{
						//Show Success
						location.reload();
		     		}
			 		else
			 		{
			 			//Show Error
			 			alert(data.msg);
		     		}
		     
			 		//Enable Button
			 		$('#login_form #btnSubmit').prop( 'disabled', false );
		            $('#login_form #btnSubmit').val('Enviar');
		    	});
		   	}  
		   
		   	return false;
		  });
		  
		//Validation Crear
		
		$("#register1_form").validate({
		   rules: {
		       inputEmail: {
			   		required: true,
			   		email: true
		       },
		       inputPassword: {
		        	required: true
		       },
		       inputName: {
			       required: true,
		       },
		       inputRol: {
			       required: true,
		       },
		       inputDependencia: {
			       required: true,
		       },
		       inputPuesto: {
			       required: true,
		       }
		   	}
		  });
		  
		  // #login_form #btnSubmit click
		  $('#register1_form #btnCreate').on('click', function(e) {
		   	e.preventDefault();
		   
		   	if ($('#register1_form').valid())
		   	{
		    	//Leemos los Valores
		    	var rol = $('#rol').val();
				var email = $('#inputEmail').val();
				var password = $('#inputPassword').val();
				var nombre = $('#inputName').val();
				var rol = $('#inputRol').val();
				var dependencia = $('#inputDependencia').val();
				var puesto = $('#inputPuesto').val();
				var param = '{"msg": "createUser","fields": {"nombre": "' + nombre + '","rol": "' + rol + '","email": "' + email + '","password": "' + password + '","dependencia": "' + dependencia + '","puesto": "' + puesto + '"}}';
				
				//Verificamos el Correo Electrónico
				$('#register1_form #btnCreate').prop( 'disabled', true );
				$('#register1_form #btnCreate').val('PROCESANDO...');
		   
				//Request API
				$.post(base_url + '/api', { param: param }).done(function( data ) {
					console.log(data);
					
					//Check Result
					if (data.status == 1)
					{
						//Show Success
						
				    	window.location.href = base_url + '/' + rol + '/usuarios/';
		     		}
			 		else
			 		{
			 			//Show Error
			 			alert(data.msg);
		     		}
		     
			 		//Enable Button
			 		$('#register1_form #btnCreate').prop( 'disabled', false );
		            $('#register1_form #btnCreate').val('Crear');
		    	});
		   	}  
		   
		   	return false;
		  });
		  
		//Validation Editar
		
		$("#edit_form").validate({
		   rules: {
		       inputEmail: {
			   		required: true,
			   		email: true
		       },
		       inputName: {
			       required: true,
		       },
		       inputRol: {
			       required: true,
		       },
		       inputDependencia: {
			       required: true,
		       },
		       inputPuesto: {
			       required: true,
		       }
		   	}
		  });
		  
		  // #edit_form #btnGuardar click
		  $('#edit_form #btnGuardar').on('click', function(e) {
		   	e.preventDefault();
		   	
		   	if ($('#edit_form').valid())
		   	{
		    	//Leemos los Valores
		    	var rol = $('#rol').val();
				var email = $('#inputEmail').val();
				var password = $('#inputPassword').val();
				var nombre = $('#inputName').val();
				var rol = $('#inputRol').val();
				var dependencia = $('#inputDependencia').val();
				var puesto = $('#inputPuesto').val();
				var param = '{"msg": "editUser","fields": {"nombre": "' + nombre + '","rol": "' + rol + '","email": "' + email + '","password": "' + password + '","dependencia": "' + dependencia + '","puesto": "' + puesto + '"}}';
				console.log(param);
				//Verificamos el Correo Electrónico
				$('#edit_form #btnGuardar').prop( 'disabled', true );
				$('#edit_form #btnGuardar').val('PROCESANDO...');
		   
				//Request API
				$.post(base_url + '/api', { param: param }).done(function( data ) {
					console.log(data);
					
					//Check Result
					if (data.status == 1)
					{
						//Show Success
						
				    	window.location.href = base_url + '/' + rol + '/usuarios/';
		     		}
			 		else
			 		{
			 			//Show Error
			 			alert(data.msg);
		     		}
		     
			 		//Enable Button
			 		$('#edit_form #btnGuardar').prop( 'disabled', false );
		            $('#edit_form #btnGuardar').val('Guardar');
		    	});
		   	}  
		   
		   	return false;
		  });
		  
		//Mover Lista
		function attachDragDrop() {
			$('.sortable').sortable();
			$('.handles').sortable({
				handle: '.contenedor-cruz'
			});
			$('.handles_child').sortable({
				handle: '.contenedor-cruz'
			});
			$('.connected').sortable({
				connectWith: '.connected'
			});
			$('.exclude').sortable({
				items: ':not(.disabled)'
			});
		}
		attachDragDrop();
		
		//btnAddColumnForm Click
		$('#btnAddColumnForm').on('click', function(e) {
			e.preventDefault();
			var count = $('.handles li').length;
			
			//append
			$('.handles').append('<li class="ms-light font14 marTop10"><div class="col-1 contenedor-cruz inline align-middle"><i class="fas fa-arrows-alt cruz-gestion c-green font20"></i></div><div class="col-4 input-group inline"><fieldset><input type="text" name="region[]" class="form-control ms-light font14 editListaUsuario" placeholder="Región"></fieldset></div><div class="col-4 input-group inline"><fieldset><select name="publicidad[]" class="form-control ms-light font14 editListaUsuario"><option value="1">Público</option><option value="0">No Público</option></select></fieldset></div><div class="col-2 text-center inline"><button id="btnRemoveColumnForm" name="btnRemoveColumnForm" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="'+count+'" >Eliminar</button></div><ul class="handles"></li>');
			attachDragDrop();
			
			return false;
		});
		
		//btnRemoveColumnForm Click
		$(document).on('click', '#btnRemoveColumnForm', function(e) {
			e.preventDefault();
			
			$(this).closest('li').remove();
			
			return false;
		});
		
		//chooseUserForm Click
		$('#chooseUserForm').on('click', function(e) {
			e.preventDefault();
			
			$('.users').append('<li class="ms-light font14 marTop10"><div class="col-7 input-group inline"><fieldset disabled=""><input type="text" name="user[]" class="form-control ms-light font14 editListaUsuario" placeholder="Usuario" rel="' + $('#selectUserForm').val() + '" value="' + $('#selectUserForm option:selected').text() + '"></fieldset></div><div class="col-2 text-center inline"><button id="btnRemoveUserForm" name="btnRemoveUserForm" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >Eliminar</button></div></li>');
			
			return true;
		});
		
		//btnRemoveUserForm Click
		$(document).on('click', '#btnRemoveUserForm', function(e) {
			e.preventDefault();
			
			$(this).closest('li').remove();
			
			return false;
		});
		
		//btnSaveForm Click
		$('#btnSaveForm').on('click', function(e) {
			e.preventDefault();
			
			var rol = $('#rol').val();
			var titulo = $('#titleForm').val();
			var unidad = $('#unidadForm').val();
			var usuarios = JSON.stringify($("input[name='user[]']").map(function(){return $(this).val() + '@' + $(this).attr('rel');}).get());
			var regiones = JSON.stringify($("input[name='region[]']").map(function(){return $(this).val();}).get());
			var publicidad = JSON.stringify($("select[name='publicidad[]']").map(function(){return $(this).val();}).get());
			var param = '{"msg": "createForm","fields": {"titulo": "' + titulo + '","unidad": "' + unidad + '","usuarios": ' + usuarios + ',"regiones": ' + regiones + ',"publicidad": ' + publicidad + '}}';
			console.log(param);
			
			if (usuarios != '[]' && regiones != '[]' && publicidad != '[]')
			{
				//Verificamos el Correo Electrónico
				$('#btnSaveForm').prop( 'disabled', true );
				$('#btnSaveForm').val('PROCESANDO...');
		   
				//Request API
				$.post(base_url + '/api', { param: param }).done(function( data ) {
					console.log(data);
					
					//Check Result
					if (data.status == 1)
					{
						//Show Success
						window.location.href = base_url + '/' + rol + '/gestion-de-formatos/';
		     		}
			 		else
			 		{
			 			//Show Error
			 			alert(data.msg);
		     		}
		     
			 		//Enable Button
			 		$('#btnSaveForm').prop( 'disabled', false );
		            $('#btnSaveForm').val('GUARDAR');
		    	});	
		    }
		    else
		    {
			    alert('Debes de seleccionar mínimo un usuario y agregar mínimo una columna');
		    }		
			
			return false;
		});
		
		//btnUpdateForm Click
		$('#btnUpdateForm').on('click', function(e) {
			e.preventDefault();
			
			var idform = $('#idform').val();
			var rol = $('#rol').val();
			var titulo = $('#titleForm').val();
			var unidad = $('#unidadForm').val();
			var usuarios = JSON.stringify($("input[name='user[]']").map(function(){return $(this).val() + '@' + $(this).attr('rel');}).get());
			var regiones = JSON.stringify($("input[name='region[]']").map(function(){return $(this).val();}).get());
			var publicidad = JSON.stringify($("select[name='publicidad[]']").map(function(){return $(this).val();}).get());
			var param = '{"msg": "updateForm","fields": {"id": "' + idform + '", "titulo": "' + titulo + '","unidad": "' + unidad + '","usuarios": ' + usuarios + ',"regiones": ' + regiones + ',"publicidad": ' + publicidad + '}}';
			console.log(param);
			
			if (usuarios != '[]' && regiones != '[]' && publicidad != '[]')
			{
				//Verificamos el Correo Electrónico
				$('#btnUpdateForm').prop( 'disabled', true );
				$('#btnUpdateForm').val('PROCESANDO...');
		   
				//Request API
				$.post(base_url + '/api', { param: param }).done(function( data ) {
					console.log(data);
					
					//Check Result
					if (data.status == 1)
					{
						//Show Success
						window.location.href = base_url + '/' + rol + '/gestion-de-formatos/';
		     		}
			 		else
			 		{
			 			//Show Error
			 			alert(data.msg);
		     		}
		     
			 		//Enable Button
			 		$('#btnUpdateForm').prop( 'disabled', false );
		            $('#btnUpdateForm').val('GUARDAR');
		    	});	
		    }
		    else
		    {
			    alert('Debes de seleccionar mínimo un usuario y agregar mínimo una columna');
		    }		
			
			return false;
		});
		
		//btnUploadCSV Click
		$('#btnUploadCSV').on('click', function(e) {
			e.preventDefault();
			
			$('#csvFile').trigger('click');
			
			return false;
		});
		
		//csvFile change
		$('#csvFile').on('change', function(e) {
			e.preventDefault();
			var ext = $("#csvFile").val().split('.').pop();
			
			if (ext == 'csv')
			{
				//Mostramos el Modal
				$('#ModalSubir').modal('show');
			}
			else
			{
				//Mostramos el Error
				alert('Debes seleccionar un archivo .csv');
			}
			
			return false;
		});
		
		//btnSaveCSV Click
		$('#btnSaveCSV').on('click', function(e) {
			e.preventDefault();
			
			var titulo = $('#titulo_csv').val();
			var fecha = $('#fecha_csv').val();
			var csv = $("#csvFile").val();
			
			//Verificamos
			if (titulo && fecha && csv)
			{
				$('#formUploadCSV').submit();
			}
			else
			{
				alert('Tienes que llenar todos los campos y elegir un CSV.');
			}
			
			return false;
		});
		
		//enviarNotificacion click
		$("#enviarNotificacion").click(function(e){
			e.preventDefault();
			
			var formato_id = $('#formato_id').val();
			var usuario_id = $('#usuario_id').val();
			var titulo = $('#notificacionTitulo').val();
			var mensaje = $('#notificacionMensaje').val();
			var param = '{"msg": "createNotification","fields": {"formato_id": "' + formato_id + '", "usuario_id": "' + usuario_id + '", "titulo": "' + titulo + '","mensaje": "' + mensaje + '"}}';
			
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
				console.log(data);
				
				//Check Result
				if (data.status == 1)
				{
					$('#notificacionResultado').html('Notificación enviada con éxito.');
					$(".footer-notificacion, .body-notificacion").hide();
					$(".body-notificacion-aceptar, .footer-notificacion-aceptar").show();
	     		}
		 		else
		 		{
		 			//Show Error
		 			$('#notificacionResultado').html(data.msg);
		 			$(".footer-notificacion, .body-notificacion").hide();
					$(".body-notificacion-aceptar, .footer-notificacion-aceptar").show();
	     		}
	    	});
			
			return false;	
		});
		
		//guardarAprobacion click
		$('#guardarAprobacion').on('click', function(e) {
			e.preventDefault();
			
			var formato_id = $('#formato_id').val();
			var usuario_id = $('#usuario_id').val();
			var post_id = $('#post_id').val();
			var param = '{"msg": "approveData","fields": {"formato_id": "' + formato_id + '", "usuario_id": "' + usuario_id + '", "post_id": "' + post_id + '"}}';
			
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
				console.log(data);
				
				//Check Result
				if (data.status == 1)
				{
					$('#aprobacionResultado').html('Tu publicación ha sido<br>aprobada con éxito.');
					$('#btPublicar').prop('disabled', true);
					$(".footer-aprobar, .body-aprobar").hide();
					$(".body-aprobar-aceptar, .footer-aprobar-aceptar").show();
	     		}
		 		else
		 		{
		 			//Show Error
		 			$('#aprobacionResultado').html(data.msg);
		 			$(".footer-aprobar, .body-aprobar").hide();
		 			$(".body-aprobar-aceptar, .footer-aprobar-aceptar").show();
	     		}
	    	});
	    	
			return false;
		});
		
		//aprobar de forma interna click
		$('.aprobarInterna').on('click', function(e) {
			e.preventDefault();
			
			var formato_id = $(this).attr('formato_id');
			var usuario_id = $(this).attr('usuario_id');
			var post_id = $(this).attr('post_id');
			var param = '{"msg": "approveData","fields": {"formato_id": "' + formato_id + '", "usuario_id": "' + usuario_id + '", "post_id": "' + post_id + '"}}';
			console.log(param);
			
			//Request API
			$.post(base_url + '/api', { param: param }).done(function( data ) {
				console.log(data);
				
				//Check Result
				if (data.status == 1)
				{
					$('#aprobacionResultado').html('Tu publicación ha sido<br>aprobada con éxito.');
					$('#btnAprobarInterna_'+post_id).addClass('isDisabled');
					$(".footer-aprobar, .body-aprobar").hide();
					$(".body-aprobar-aceptar, .footer-aprobar-aceptar").show();
	     		}
		 		else
		 		{
		 			//Show Error
		 			$('#aprobacionResultado').html(data.msg);
		 			$(".footer-aprobar, .body-aprobar").hide();
		 			$(".body-aprobar-aceptar, .footer-aprobar-aceptar").show();
	     		}
	    	});
	    	
			return false;
		});
		
	});