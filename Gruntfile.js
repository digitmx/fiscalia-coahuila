'use strict';
module.exports = function(grunt) {

    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({

		watch: {
		    js: {
		        files: ['js/functions.js'],
		        tasks: ['concat:js','uglify']
    		},
    		css: {
	    		files: ['style.css'],
		        tasks: ['concat:css','cssmin']
    		},
    		php: {
	    		files: ['*.php','page-templates/*.php','includes/*.php','reader/*.php'],
		        tasks: ['copy']
    		}
		},

        copy: {
		  	main: {
		    	files: [
					// includes files within path and its sub-directories
					{expand: true, src: ['style.css'], dest: 'dist/'},
					{expand: true, src: ['*.php'], dest: 'dist/'},
					{expand: true, src: ['fonts/*'], dest: 'dist/'},
					{expand: true, src: ['webfonts/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-Black/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-BlackItalic/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-Bold/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-BoldItalic/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-ExtraBold/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-ExtraBoldItalic/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-ExtraLight/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-ExtraLightItalic/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-Italic/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-Light/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-LightItalic/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-Medium/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-MediumItalic/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-Regular/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-SemiBold/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-SemiBoldItalic/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-Thin/*'], dest: 'dist/'},
					{expand: true, src: ['fonts/Montserrat-ThinItalic/*'], dest: 'dist/'},
					{expand: true, src: ['screenshot.png'], dest: 'dist/'},
					{expand: true, src: ['js/html5shiv-printshiv.js'], dest: 'dist/'},
					{expand: true, src: ['js/respond.min.js'], dest: 'dist/'},
					//{expand: true, src: ['js/*'], dest: 'dist/'},
					{expand: true, src: ['page-templates/*'], dest: 'dist/'},
					//{expand: true, src: ['databases/*'], dest: 'dist/'},
					//{expand: true, src: ['reader/*'], dest: 'dist/'},
					{expand: true, src: ['includes/*'], dest: 'dist/'},
					//{expand: true, src: ['css/ajax-loader.gif'], dest: 'dist/'},
					//{expand: true, src: ['css/fonts/*'], dest: 'dist/'},
					//{expand: true, src: ['css/*'], dest: 'dist/'},
					//{expand: true, src: ['favicon/*'], dest: 'dist/'},
				],
		  	},
		},

        concat: {
            css: {
                src: [
                    'http://fonts.googleapis.com/icon?family=Material+Icons','css/bootstrap.min.css','css/all.min.css','css/jquery-ui.min.css','css/jquery-ui.theme.min.css','css/tabulator_bootstrap4.min.css','style.css'
                ],
                dest: 'combined/combined.css'
            },
            js: {
                src: [
                    'js/jquery-3.3.1.min.js','js/jquery-ui.min.js','js/popper.min.js','js/bootstrap.min.js','js/validation/jquery.validate.min.js','js/validation/additional-methods.min.js','js/validation/localization/messages_es.min.js','js/Chart.bundle.min.js','js/tabulator.min.js','js/jquery_wrapper.min.js','js/functions.js'
                ],
                dest: 'combined/combined.js'
            }
        },

        cssmin: {
            css: {
                src: 'combined/combined.css',
                dest: 'dist/css/app.css'
            },
            css_local: {
                src: 'combined/combined.css',
                dest: 'css/app.css'
            }
        },

        uglify: {
            js: {
                files: {
                    'dist/js/app.js': ['combined/combined.js']
                }
            },
            js_local: {
                files: {
                    'js/app.js': ['combined/combined.js']
                }
            }
        },

        // image optimization
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 7,
                    progressive: true
                },
                files: [{
                    expand: true,
                    cwd: 'images/',
                    src: '**/*',
                    dest: 'dist/img/'
                },
                {
                    expand: true,
                    cwd: 'images/',
                    src: '**/*',
                    dest: 'img/'
                }]
            }
        },

        clean: ["dist/"]

    });

    // register task
    grunt.registerTask('default', ['clean', 'imagemin', 'concat', 'cssmin', 'uglify', 'copy']);

};