<?php /* Template Name: Descargar CSV */ ?>
<?php
	//Leer Datos
	$id = (isset($_GET['id'])) ? (string)trim($_GET['id']) : '';
	$datos = get_post( $id );
	$datos_json = get_field( "datos", $datos->ID);
	$datos_json = json_decode($datos_json);
	
	$output = fopen("php://output",'w') or die("Can't open php://output");
	header("Content-Type:application/csv"); 
	header("Content-Disposition:attachment;filename=export.csv"); 
	foreach($datos_json as $row) {
	    fputcsv($output, $row);
	}
	fclose($output) or die("Can't close php://output");
	
?>