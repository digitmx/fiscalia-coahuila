<?php /* Template Name: Notificaciones Rol 3 */ ?>
<?php if (!isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/logout/' ); } ?>
<?php if ($_SESSION['user']['rol'] != 'rol-3') { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia-rol3"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative">
						NOTIFICACIONES
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<?php
			//Query News
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'posts_per_page'   => 10,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'notificacion',
				'post_status'      => 'publish',
				'paged'			   => $paged,
				'suppress_filters' => false 
			);
			$query = new WP_Query( $args );
		?>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-texto-fiscalia-coahuila">
						<?php while ( $query->have_posts() ) : $query->the_post(); setup_postdata( $post ); ?>
						<div class="not-tabla p-relative">
							<span class="ms-medium font20 tabla-line">
								<?php echo get_field("titulo", $post->ID); ?>
							</span>
							<br>
							<?php $usuario = get_field("usuario", $post->ID); ?>
							<span class="ms-medium font12 tabla-date">
								<i class="far fa-user c-green"></i>&nbsp;&nbsp;<?php echo get_field("nombre",$usuario); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-calendar c-green"></i>&nbsp;&nbsp;<?php the_time( 'j / M / Y' ); ?>
							</span>
							<div class="p-absolute pos-btnMas">
								<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/notificaciones/ver/?id=<?php echo $post->ID; ?>" class="no-underline">
									<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas" rel="1">
									Ver Más
									</button>
								</a>
							</div>
						</div>
						<?php endwhile; wp_reset_postdata(); ?>
						<!--
						<div class="not-tabla p-relative">
							<span class="ms-medium font20 tabla-line">
								Gestor General subio formato .CSV
							</span>
							<br>
							<span class="ms-medium font12 tabla-date">
								<i class="far fa-user c-green"></i>&nbsp;&nbsp;Rol 4 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-calendar c-green"></i>&nbsp;&nbsp;09 / Sep / 2018
							</span>
							<div class="p-absolute pos-btnMas">
								<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas" rel="1">
								Ver Más
								</button>
							</div>
						</div>
						<div class="not-tabla p-relative">
							<span class="ms-medium font20 tabla-line">
								Gestor General subio formato .CSV
							</span>
							<br>
							<span class="ms-medium font12 tabla-date">
								<i class="far fa-user c-green"></i>&nbsp;&nbsp;Rol 3 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-calendar c-green"></i>&nbsp;&nbsp;08 / Sep / 2018
							</span>
							<div class="p-absolute pos-btnMas">
								<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas" rel="1">
								Ver Más
								</button>
							</div>
						</div>
						<div class="not-tabla p-relative">
							<span class="ms-medium font20 tabla-line">
								Gestor General subio formato .CSV
							</span>
							<br>
							<span class="ms-medium font12 tabla-date">
								<i class="far fa-user c-green"></i>&nbsp;&nbsp;Rol 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-calendar c-green"></i>&nbsp;&nbsp;07 / Sep / 2018
							</span>
							<div class="p-absolute pos-btnMas">
								<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas" rel="1">
								Ver Más
								</button>
							</div>
						</div>
						<div class="not-tabla p-relative">
							<span class="ms-medium font20 tabla-line">
								Gestor General subio formato .CSV
							</span>
							<br>
							<span class="ms-medium font12 tabla-date">
								<i class="far fa-user c-green"></i>&nbsp;&nbsp;Rol 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-calendar c-green"></i>&nbsp;&nbsp;06 / Sep / 2018
							</span>
							<div class="p-absolute pos-btnMas">
								<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas" rel="1">
								Ver Más
								</button>
							</div>
						</div>
						<div class="not-tabla p-relative">
							<span class="ms-medium font20 tabla-line">
								Gestor General subio formato .CSV
							</span>
							<br>
							<span class="ms-medium font12 tabla-date">
								<i class="far fa-user c-green"></i>&nbsp;&nbsp;Rol 2 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-calendar c-green"></i>&nbsp;&nbsp;05 / Sep / 2018
							</span>
							<div class="p-absolute pos-btnMas">
								<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas" rel="1">
								Ver Más
								</button>
							</div>
						</div>
						<div class="not-tabla p-relative">
							<span class="ms-medium font20 tabla-line">
								Gestor General subio formato .CSV
							</span>
							<br>
							<span class="ms-medium font12 tabla-date">
								<i class="far fa-user c-green"></i>&nbsp;&nbsp;Rol 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-calendar c-green"></i>&nbsp;&nbsp;04 / Sep / 2018
							</span>
							<div class="p-absolute pos-btnMas">
								<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas" rel="1">
								Ver Más
								</button>
							</div>
						</div>
						<div class="not-tabla p-relative">
							<span class="ms-medium font20 tabla-line">
								Gestor General subio formato .CSV
							</span>
							<br>
							<span class="ms-medium font12 tabla-date">
								<i class="far fa-user c-green"></i>&nbsp;&nbsp;Rol 1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-calendar c-green"></i>&nbsp;&nbsp;04 / Sep / 2018
							</span>
							<div class="p-absolute pos-btnMas">
								<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas" rel="1">
								Ver Más
								</button>
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>