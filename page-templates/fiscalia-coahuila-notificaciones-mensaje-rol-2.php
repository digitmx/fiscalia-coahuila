<?php /* Template Name: Notificaciones Mensajes Rol 2 */ ?>
<?php if (!isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/logout/' ); } ?>
<?php if ($_SESSION['user']['rol'] != 'rol-2') { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php $id = (isset($_GET['id'])) ? (string)trim($_GET['id']) : ''; ?>
<?php if ($id) { $notificacion = get_post($id); } else { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php if (get_field("usuario", $notificacion->ID)) { $usuario = get_post(get_field("usuario", $notificacion->ID)); } ?>
<?php get_header(); ?>

	<?php setup_postdata( $notificacion ); ?>
	
	<?php get_template_part("includes/navbar","fiscalia-rol2"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/notificaciones/" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						TITULO DE MENSAJE DE <?php echo get_field("rol", $usuario->ID); ?>
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-texto-fiscalia-coahuila-mensaje">
						<div class="not-tabla-mensaje p-relative">
							<span class="ms-medium font20">
								<?php echo get_field("titulo", $notificacion->ID); ?>
							</span>
						</div>
						<div class="ms-medium font14 tabla-date-mensaje">
							<i class="far fa-user c-green font20"></i>&nbsp;&nbsp;<?php echo get_field("nombre",$usuario->ID); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="far fa-calendar c-green font20"></i>&nbsp;&nbsp;<?php echo get_the_time('j / M / Y', $notificacion->ID); ?>
						</div>
						<div class="speech-bubble">
							<p class="ms-regular font14">
								<?php echo get_field("mensaje", $notificacion->ID); ?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php wp_reset_postdata(); ?>

<?php get_footer(); ?>