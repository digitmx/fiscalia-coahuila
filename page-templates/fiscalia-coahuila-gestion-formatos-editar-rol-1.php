<?php /* Template Name: Gestión Formatos Editar Rol 1 */ ?>
<?php if (!isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/logout/' ); } ?>
<?php if ($_SESSION['user']['rol'] != 'rol-1') { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php $id = (isset($_GET['id'])) ? (string)trim($_GET['id']) : ''; ?>
<?php $post = get_post($id); ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/rol-1/gestion-de-formatos/" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						Editar Formato
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-tabla-fiscalia-coahuila">
						<div class="container-fluid">
							<div class="row marTop20 marBot20 p-relative">
								<div class="col-6 text-white">
									<span class="ms-medium font10 c-green marLef20">
										Titulo
									</span>
									<div class="input-group marTop10">
										<input type="text" id="titleForm" name="titleForm" class="form-control ms-light font14 editListaUsuario" placeholder="Nombre del titulo" value="<?php echo get_field("titulo", $post->ID); ?>">
									</div>
								</div>
								<!--<div class="col-4 text-white">
									<span class="ms-medium font10 c-green marLef20">
										Rol
									</span>
									<div class="input-group marTop10">
										<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Rol 1">
									</div>
								</div>-->
								<div class="col-6 text-white">
									<span class="ms-medium font10 c-green marLef20">
										Unidad
									</span>
									<div class="input-group marTop10">
										<input type="text" id="unidadForm" name="unidadForm" class="form-control ms-light font14 editListaUsuario" placeholder="Nombre de unidad" value="<?php echo get_field("unidad", $post->ID); ?>">
									</div>
								</div>
								<div class="col-12 p-relative">
									<div class="col-2 no-padding float-right">
										<div class="text-center padTop10">
											<button id="" type="button" data-toggle="modal" data-target="#ModalCrear" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnUser" rel="1">
											Agregar Usuario
											</button>
											<!-- Modal Notificacion -->
											<div class="modal fade" id="ModalCrear" tabindex="-1" role="dialog" aria-labelledby="ModalNotificarTitle" aria-hidden="true">
												<div class="modal-dialog modal-dialog-centered" role="document">
													<div class="modal-content">
														<div class="modal-header justify-content-center">
															<h5 class="modal-title ms-semibold font16" id="ModalNotificarTitle">Crear Formato de Usuario</h5>
														</div>
														<div class="modal-body">
															<div class="container-fluid body-notificacion">
																<div class="row">
																	<div class="col text-white">
																		<span class="ms-medium font10 float-left c-green marLef20">
																			Usuario
																		</span>
																		<?php
																			$args = array(
																				'post_type' => 'usuario',
																				'posts_per_page' => -1,
																				'post__not_in' => array($_SESSION['user']['id']),
																				'post_status' => 'publish'
																			);
																			$query = get_posts($args);
																		?>
																		<form>
																			<div class="input-group marTop10">
																				<div class="input-field col s12 no-padding">
																					<select id="selectUserForm" name="selectUserForm" class="form-control ms-light font14 editListaUsuario">
																						<?php foreach ($query as $row) { ?>
																						<option value="<?php echo $row->ID; ?>"><?php echo get_field("nombre", $row->ID); ?></option>
																						<?php } ?>
																					</select>
																				</div>
																			</div>
																		</form>
																	</div>
																</div>
															</div>
															<!--<div class="container body-notificacion-aceptar">
																<p class="ms-light font24 text-center pos-Notificacion">
																	Formato de Usuario Creado con Exito.
																</p>
															</div>-->
														</div>
														<div class="modal-footer justify-content-center">
															<div class="col footer-notificacion">
																<div class="text-center">
																	<button id="chooseUserForm" name="chooseUserForm" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" data-dismiss="modal">
																	Seleccionar
																	</button>
																</div>
															</div>
															<div class="col footer-notificacion">
																<div class="text-center">
																	<button id="cerrar-crear" type="button" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" data-dismiss="modal">
																	Cancelar
																	</button>
																</div>
															</div>
															<!--<div class="col-6 footer-notificacion-aceptar">
																<div class="text-center">
																	<button id="aceptar-crear" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" data-dismiss="modal">
																	Aceptar
																	</button>
																</div>
															</div>-->
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php $usuarios_json = json_decode(get_field("usuarios", $post->ID)); ?>
						<ul class="users">
							<?php foreach ($usuarios_json as $usuario_row) { ?>
							<li class="ms-light font14 marTop10">
								<div class="col-7 input-group inline">
									<fieldset disabled="">
										<input type="text" name="user[]" class="form-control ms-light font14 editListaUsuario" placeholder="Usuario" rel="<?php echo $usuario_row->id; ?>" value="<?php echo $usuario_row->name; ?>">
									</fieldset>
								</div>
								<div class="col-2 text-center inline">
									<button id="btnRemoveUserForm" name="btnRemoveUserForm" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1">Eliminar</button>
								</div>
							</li>
							<?php } ?>
						</ul>
						<table class="table table-borderless table-responsive tabla-responsiva">
							<thead>
								<tr>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla padLeft75rem">Titulo</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla padLeft75rem">Publicidad</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
								</tr>
							</thead>
						</table>
						<?php $campos_json = json_decode(get_field("campos", $post->ID)); ?>
						<ul class="handles">
							<?php foreach ($campos_json as $campo_row) { ?>
							<li class="ms-light font14 marTop10" draggable="true">
								<div class="col-1 contenedor-cruz inline align-middle">
									<svg class="svg-inline--fa fa-arrows-alt fa-w-16 cruz-gestion c-green font20" aria-hidden="true" data-prefix="fas" data-icon="arrows-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
										<path fill="currentColor" d="M352.201 425.775l-79.196 79.196c-9.373 9.373-24.568 9.373-33.941 0l-79.196-79.196c-15.119-15.119-4.411-40.971 16.971-40.97h51.162L228 284H127.196v51.162c0 21.382-25.851 32.09-40.971 16.971L7.029 272.937c-9.373-9.373-9.373-24.569 0-33.941L86.225 159.8c15.119-15.119 40.971-4.411 40.971 16.971V228H228V127.196h-51.23c-21.382 0-32.09-25.851-16.971-40.971l79.196-79.196c9.373-9.373 24.568-9.373 33.941 0l79.196 79.196c15.119 15.119 4.411 40.971-16.971 40.971h-51.162V228h100.804v-51.162c0-21.382 25.851-32.09 40.97-16.971l79.196 79.196c9.373 9.373 9.373 24.569 0 33.941L425.773 352.2c-15.119 15.119-40.971 4.411-40.97-16.971V284H284v100.804h51.23c21.382 0 32.09 25.851 16.971 40.971z"></path>
									</svg><!-- <i class="fas fa-arrows-alt cruz-gestion c-green font20"></i> -->
								</div>
								<div class="col-4 input-group inline">
									<fieldset>
										<input type="text" name="region[]" class="form-control ms-light font14 editListaUsuario" placeholder="Región" value="<?php echo preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $campo_row->campo); ?>">
									</fieldset>
								</div>
								<div class="col-4 input-group inline">
									<fieldset>
										<?php $publicidad = preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $campo_row->publicidad); ?>
										<select name="publicidad[]" class="form-control ms-light font14 editListaUsuario">
											<option <?=($publicidad=='1') ? ' selected="" ' : ''; ?> value="1">Público</option>
											<option <?=($publicidad=='0') ? ' selected="" ' : ''; ?> value="0">No Público</option>
										</select>
									</fieldset>
								</div>
								<div class="col-2 text-center inline">
									<button id="btnRemoveColumnForm" name="btnRemoveColumnForm" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1">Eliminar</button>
								</div>
							</li>
							<?php } ?>
						</ul>
						<div class="col text-center border-line">
							<div class="bloque-tabla">
								<div class="col">
									<div class="text-center">
										<button id="btnAddColumnForm" name="btnAddColumnForm" type="submit" class="btn btn-lg btn-block ms-bold font11 btnVerMas" rel="1" >
										AGREGAR COLUMNA
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row justify-content-center btnGuardarEditar">
						<div class="col col-lg-10">
							<div class="row justify-content-center">
								<!--
								<div class="col-6 col-lg-3">
									<div class="text-center">
										<button id="btDesactivar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
										DESACTIVAR
										</button>
									</div>
								</div>
								-->
								<div class="col-6 col-lg-3">
									<input type="hidden" id="idform" name="idform" value="<?php echo $id; ?>" />
									<input type="hidden" id="rol" name="rol" value="<?php echo $_SESSION['user']['rol']; ?>" />
									<div class="text-center">
										<button id="btnUpdateForm" name="btnUpdateForm" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" rel="1" >
										ACTUALIZAR 
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>