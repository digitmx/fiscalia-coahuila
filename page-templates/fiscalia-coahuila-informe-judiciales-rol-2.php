<?php /* Template Name: Informe Mandamientos Judiciales Rol 2 */ ?>
<?php if (!isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/logout/' ); } ?>
<?php if ($_SESSION['user']['rol'] != 'rol-2') { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php $id = (isset($_GET['id'])) ? (string)trim($_GET['id']) : ''; ?>
<?php if ($id) { $formato = get_post($id); } else { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia-rol2"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						<?php echo get_field("titulo", $formato->ID); ?>
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<?php
			//Query News
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'posts_per_page'   => 10,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'dato',
				'post_status'      => 'publish',
				'paged'			   => $paged,
				'suppress_filters' => false,
				'meta_query' => array(
					array(
						'key' => 'formato',
						'value' => $formato->ID,
						'compare' => '='
					)
				)
			);
			$query = new WP_Query( $args );
		?>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-tabla-fiscalia-coahuila">
						<table class="table table-borderless table-striped table-responsive tabla-responsiva">
							<thead>
								<tr>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla">Titulo</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla padLeft75rem">Fecha</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
								</tr>
							</thead>
							<tbody>
								<?php while ( $query->have_posts() ) : $query->the_post(); setup_postdata( $post ); ?>
								<tr>
									<th scope="row" class="ms-regular font14"><?php echo get_field("titulo", $post->ID); ?></th>
									<td class="ms-regular font14"><?php echo get_the_time('j / M / Y', $post->ID); ?></td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/orden/ver-csv/?id=<?php echo $post->ID; ?>" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/orden/editar-tabla/?id=<?php echo $post->ID; ?>" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>	
									<td><a id="btnAprobarInterna_<?php echo $post->ID; ?>" href="#" data-toggle="modal" data-target="#ModalAprobar_<?php echo $post->ID; ?>" class="float-left ms-bold font11 c-green uppercase padTop5<?=(get_field("aprobado", $post->ID)) ? ' isDisabled' : '';?>">Publicar de Forma Interna</a></td>
								</tr>
								<!-- Modal Notificacion -->						
								<div class="modal fade" id="ModalAprobar_<?php echo $post->ID; ?>" tabindex="-1" role="dialog" aria-labelledby="ModalAprobarTitle" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header justify-content-center">
												<h5 class="modal-title ms-semibold font16" id="ModalAprobarTitle"><?php echo get_field("titulo", $formato->ID); ?></h5>
											</div>
											<div class="modal-body">
												<div class="container-fluid body-aprobar">
													<div class="container body-aprobar-guardar">
														<p class="ms-light font24 text-center pos-Notificacion">
															¿Esta seguro de aprobar la publicación?
														</p>
													</div>
												</div>
												<div class="container body-aprobar-aceptar">
													<p class="ms-light font24 text-center pos-Notificacion" id="aprobacionResultado"></p>
												</div>
											</div>
											<div class="modal-footer justify-content-center">
												<div class="col footer-aprobar">
													<div class="text-center">
														<button formato_id="<?php echo $formato->ID; ?>" usuario_id="<?php echo $_SESSION['user']['id']; ?>" post_id="<?php echo $post->ID; ?>" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv aprobarInterna">
															Guardar
														</button>
													</div>
												</div>
												<div class="col footer-aprobar">
													<div class="text-center">
														<button id="cerrar-apr" type="button" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" data-dismiss="modal">
														Cancelar
														</button>
													</div>
												</div>
												<div class="col-6 footer-aprobar-aceptar">
													<div class="text-center">
														<button id="aceptar-apr" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" data-dismiss="modal">
														Aceptar
														</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php endwhile; wp_reset_postdata(); ?>
								<!--
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/editar-tabla/" class="ms-bold font11 c-green uppercase padTop5">Editar</a></td>
									<td></div><a href="#" data-toggle="modal" data-target="#ModalAprobar" class="float-left ms-bold font11 c-green uppercase padTop5">Publicar de Forma Interna</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/editar-tabla/" class="ms-bold font11 c-green uppercase padTop5">Editar</a></td>
									<td></div><a href="#" data-toggle="modal" data-target="#ModalAprobar" class="float-left ms-bold font11 c-green uppercase padTop5">Publicar de Forma Interna</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/editar-tabla/" class="ms-bold font11 c-green uppercase padTop5">Editar</a></td>
									<td></div><a href="#" data-toggle="modal" data-target="#ModalAprobar" class="float-left ms-bold font11 c-green uppercase padTop5">Publicar de Forma Interna</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/editar-tabla/" class="ms-bold font11 c-green uppercase padTop5">Editar</a></td>
									<td></div><a href="#" data-toggle="modal" data-target="#ModalAprobar" class="float-left ms-bold font11 c-green uppercase padTop5">Publicar de Forma Interna</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/editar-tabla/" class="ms-bold font11 c-green uppercase padTop5">Editar</a></td>
									<td></div><a href="#" data-toggle="modal" data-target="#ModalAprobar" class="float-left ms-bold font11 c-green uppercase padTop5">Publicar de Forma Interna</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/editar-tabla/" class="ms-bold font11 c-green uppercase padTop5">Editar</a></td>
									<td></div><a href="#" data-toggle="modal" data-target="#ModalAprobar" class="float-left ms-bold font11 c-green uppercase padTop5">Publicar de Forma Interna</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/editar-tabla/" class="ms-bold font11 c-green uppercase padTop5">Editar</a></td>
									<td></div><a href="#" data-toggle="modal" data-target="#ModalAprobar" class="float-left ms-bold font11 c-green uppercase padTop5">Publicar de Forma Interna</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/editar-tabla/" class="ms-bold font11 c-green uppercase padTop5">Editar</a></td>
									<td></div><a href="#" data-toggle="modal" data-target="#ModalAprobar" class="float-left ms-bold font11 c-green uppercase padTop5">Publicar de Forma Interna</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/editar-tabla/" class="ms-bold font11 c-green uppercase padTop5">Editar</a></td>
									<td></div><a href="#" data-toggle="modal" data-target="#ModalAprobar" class="float-left ms-bold font11 c-green uppercase padTop5">Publicar de Forma Interna</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td class="p-relative text-center"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/rol-2/informacion-estadistica/orden/editar-tabla/" class="ms-bold font11 c-green uppercase padTop5">Editar</a></td>
									<td></div><a href="#" data-toggle="modal" data-target="#ModalAprobar" class="float-left ms-bold font11 c-green uppercase padTop5">Publicar de Forma Interna</a></td>
								</tr>
								-->
							</tbody>
						</table>
						<!--
						<div class="col text-center border-line">
							<div class="bloque-tabla">
								<form class="form-inline ms-medium font11">
									<label class="my-1 mr-2" for="listaUsuarios">Articulos por página:</label>
									<select class="custom-select my-1 mr-sm-2 btEditListUser" id="listaUsuarios">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option selected  value="10">10</option>
									</select>
									<div class="col">
										<span class="">2 - 10 de 30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
										<a href="#"><i class="fas fa-chevron-left c-green font14"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="#"><i class="fas fa-chevron-right font14 c-green"></i></a>
									</div>
								</form>
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>