<?php /* Template Name: Informe Mandamientos Judiciales Rol 4 */ ?>
<?php $id = (isset($_GET['id'])) ? (string)trim($_GET['id']) : ''; ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia-rol4"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/rol-4/informacion-estadistica/" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						LISTA DE CSV POR FORMATO
						<div class="col-auto no-padding p-absolute btn-CrearUsuario">
							<div class="text-center padTop10">
								<a href="<?php bloginfo("url"); ?>/rol-4/informacion-estadistica/orden/subir-informacion-estadistica/?id=<?php echo $id; ?>" class="no-inline" >
									<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnUser" rel="1">
									Subir Información Estadística
									</button>
								</a>
							</div>
						</div>
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<?php
			//Query News
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'posts_per_page'   => 10,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'dato',
				'post_status'      => 'publish',
				'paged'			   => $paged,
				'suppress_filters' => false,
				'meta_query' => array(
					array(
						'key'     => 'formato',
						'value'   => $id,
						'compare' => '=',
					),
				)
			);
			$query = new WP_Query( $args );
		?>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-tabla-fiscalia-coahuila">
						<table class="table table-borderless table-striped table-responsive tabla-responsiva">
							<thead>
								<tr>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla">Titulo</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla padLeft75rem">Fecha</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
								</tr>
							</thead>
							<tbody>
								<?php while ( $query->have_posts() ) : $query->the_post(); setup_postdata( $post ); ?>
								<?php $formato = get_field("formato", $post->ID); ?>
								<tr>
									<th scope="row" class="ms-regular font14"><?php the_field( "titulo", $post->ID ); ?></th>
									<td class="ms-regular font14"><?php the_field( "fecha", $post->ID ); ?></td>
									<td class="text-center"></div><a href="<?php bloginfo("url"); ?>/rol-4/informacion-estadistica/orden/ver/?id=<?php echo $formato; ?>" class="ms-bold font11 c-green uppercase padTop5">Ver Información Estadística</a></td>
								</tr>
								<?php endwhile; wp_reset_postdata(); ?>
							</tbody>
						</table>
						<!--
						<div class="col text-center border-line">
							<div class="bloque-tabla">
								<form class="form-inline ms-medium font11">
									<label class="my-1 mr-2" for="listaUsuarios">Articulos por página:</label>
									<select class="custom-select my-1 mr-sm-2 btEditListUser" id="listaUsuarios">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option selected  value="10">10</option>
									</select>
									<div class="col">
										<span class="">2 - 10 de 30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
										<a href="#"><i class="fas fa-chevron-left c-green font14"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="#"><i class="fas fa-chevron-right font14 c-green"></i></a>
									</div>
								</form>
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>