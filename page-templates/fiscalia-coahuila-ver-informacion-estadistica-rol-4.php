<?php /* Template Name: Ver Informacion Estadistica Rol 4 */ ?>

<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia-rol4"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/rol-4/informacion-estadistica/" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						Ver Información Estadística
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="container marTop30 marBot30">
				<div class="row justify-content-center">
					<div class="col-sm-12 col-md-10 col-lg-6">
						<div class="contenedor-texto-perfil marTop40">
							<div class="row boxFormPerfil-coa">
								<div class="col-12 text-white marTop10">
									<span class="ms-medium font10 c-green marLef20">
										Titulo
									</span>
									<div class="input-group marTop10">
										<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Nombre">
									</div>
								</div>
								<div class="col-12 text-white marTop10 marBot40">
									<span class="ms-medium font10 c-green marLef20">
										Fecha
									</span>
									<div class="input-group marTop10">
										<input type="date" class="form-control ms-light font14 editListaUsuario" placeholder="00/00/00">
									</div>
								</div>
								<div class="col">
									<div class="text-center marBot20">
										<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas" rel="1">
										Descargar CSV
										</button>
									</div>
								</div>
							</div>
							<div class="row btnGuardarEditar">
								<div class="col-6">
									<div class="text-center">
										<button id="btGuardar_01" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar" rel="1" >
										Cancelar
										</button>
									</div>
								</div>
								<div class="col-6">
									<div class="text-center">
										<button id="btEliminar_01" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar" rel="1" >
										Guardar
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>