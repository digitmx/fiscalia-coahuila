<?php /* Template Name: Gestión Formatos Crear Rol 1 */ ?>
<?php if (!isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/logout/' ); } ?>
<?php if ($_SESSION['user']['rol'] != 'rol-1') { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/rol-1/gestion-de-formatos/" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						Crear Formato
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-tabla-fiscalia-coahuila">
						<div class="container-fluid">
							<div class="row marTop20 marBot20 p-relative">
								<div class="col-6 text-white">
									<span class="ms-medium font10 c-green marLef20">
										Titulo
									</span>
									<div class="input-group marTop10">
										<input type="text" id="titleForm" name="titleForm" class="form-control ms-light font14 editListaUsuario" placeholder="Nombre del titulo">
									</div>
								</div>
								<!--<div class="col-4 text-white">
									<span class="ms-medium font10 c-green marLef20">
										Rol
									</span>
									<div class="input-group marTop10">
										<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Rol 1">
									</div>
								</div>-->
								<div class="col-6 text-white">
									<span class="ms-medium font10 c-green marLef20">
										Unidad
									</span>
									<div class="input-group marTop10">
										<input type="text" id="unidadForm" name="unidadForm" class="form-control ms-light font14 editListaUsuario" placeholder="Nombre de unidad">
									</div>
								</div>
								<div class="col-12 p-relative">
									<div class="col-2 no-padding float-right">
										<div class="text-center padTop10">
											<button id="" type="button" data-toggle="modal" data-target="#ModalCrear" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnUser" rel="1">
											Agregar Usuario
											</button>
											<!-- Modal Notificacion -->
											<div class="modal fade" id="ModalCrear" tabindex="-1" role="dialog" aria-labelledby="ModalNotificarTitle" aria-hidden="true">
												<div class="modal-dialog modal-dialog-centered" role="document">
													<div class="modal-content">
														<div class="modal-header justify-content-center">
															<h5 class="modal-title ms-semibold font16" id="ModalNotificarTitle">Crear Formato de Usuario</h5>
														</div>
														<div class="modal-body">
															<div class="container-fluid body-notificacion">
																<div class="row">
																	<div class="col text-white">
																		<span class="ms-medium font10 float-left c-green marLef20">
																			Usuario
																		</span>
																		<?php
																			$args = array(
																				'post_type' => 'usuario',
																				'posts_per_page' => -1,
																				'post__not_in' => array($_SESSION['user']['id']),
																				'post_status' => 'publish'
																			);
																			$query = get_posts($args);
																		?>
																		<form>
																			<div class="input-group marTop10">
																				<div class="input-field col s12 no-padding">
																					<select id="selectUserForm" name="selectUserForm" class="form-control ms-light font14 editListaUsuario">
																						<?php foreach ($query as $row) { ?>
																						<option value="<?php echo $row->ID; ?>"><?php echo get_field("nombre", $row->ID); ?></option>
																						<?php } ?>
																					</select>
																				</div>
																			</div>
																		</form>
																	</div>
																</div>
															</div>
															<!--<div class="container body-notificacion-aceptar">
																<p class="ms-light font24 text-center pos-Notificacion">
																	Formato de Usuario Creado con Exito.
																</p>
															</div>-->
														</div>
														<div class="modal-footer justify-content-center">
															<div class="col footer-notificacion">
																<div class="text-center">
																	<button id="chooseUserForm" name="chooseUserForm" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" data-dismiss="modal">
																	Seleccionar
																	</button>
																</div>
															</div>
															<div class="col footer-notificacion">
																<div class="text-center">
																	<button id="cerrar-crear" type="button" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" data-dismiss="modal">
																	Cancelar
																	</button>
																</div>
															</div>
															<!--<div class="col-6 footer-notificacion-aceptar">
																<div class="text-center">
																	<button id="aceptar-crear" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" data-dismiss="modal">
																	Aceptar
																	</button>
																</div>
															</div>-->
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<ul class="users">
							
						</ul>
						<table class="table table-borderless table-responsive tabla-responsiva">
							<thead>
								<tr>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla padLeft75rem">Titulo</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla padLeft75rem">Publicidad</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
								</tr>
							</thead>
						</table>
						<ul class="handles">
							
						</ul>
						<div class="col text-center border-line">
							<div class="bloque-tabla">
								<div class="col">
									<div class="text-center">
										<button id="btnAddColumnForm" name="btnAddColumnForm" type="submit" class="btn btn-lg btn-block ms-bold font11 btnVerMas" rel="1" >
										AGREGAR COLUMNA
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row justify-content-center btnGuardarEditar">
						<div class="col col-lg-10">
							<div class="row justify-content-center">
								<!--
								<div class="col-6 col-lg-3">
									<div class="text-center">
										<button id="btDesactivar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
										DESACTIVAR
										</button>
									</div>
								</div>
								-->
								<div class="col-6 col-lg-3">
									<input type="hidden" id="rol" name="rol" value="<?php echo $_SESSION['user']['rol']; ?>" />
									<div class="text-center">
										<button id="btnSaveForm" name="btnSaveForm" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" rel="1" >
										GUARDAR 
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>