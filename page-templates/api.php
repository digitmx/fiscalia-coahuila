<?php /* Template Name: API */

	//Include PHPMailer
	include_once( getcwd().'/wp-includes/class-phpmailer.php' );
	
	//Read Param Field
	$json = (isset($_POST['param'])) ? $_POST['param'] : NULL; $output = FALSE;
	$json = str_replace('\\', '', $json);

	//Check JSON
	if ($json != NULL)
	{
		//Decode Data JSON
		$json_decode = json_decode($json, true);

		//Read Action JSON
		$msg = (isset($json_decode['msg'])) ? (string)trim($json_decode['msg']) : '';

		//Read Fields JSON
		$fields = (isset($json_decode['fields'])) ? $json_decode['fields'] : array();
		
		//createUser
		if ($msg == 'createUser')
		{
			//Read Data
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			$nombre = (isset($fields['nombre'])) ? (string)trim($fields['nombre']) : '';
			$rol = (isset($fields['rol'])) ? (string)trim($fields['rol']) : '';
			$dependencia = (isset($fields['dependencia'])) ? (string)trim($fields['dependencia']) : '';
			$puesto = (isset($fields['puesto'])) ? (string)trim($fields['puesto']) : '';
			
			//Check Values
			if ($email && $password)
			{
				//Query User
				$args = array(
					'post_type' => 'usuario',
					'meta_query' => array(
						array(
							'key' => 'email',
							'value' => $email,
							'compare' => '='
						)
					)
				);
				$query = get_posts($args);
				
				//Check User exists
				if (count($query) == 0)
				{
					//Register User
					$my_post = array(
					  'post_title'    => wp_strip_all_tags($email, true),
					  'post_status'   => 'publish',
					  'post_author'   => 1,
					  'post_type'	  => 'usuario'
					);

					// Save Data
					$post_id = wp_insert_post( $my_post );

					//Verify
					if ($post_id != 0)
					{
						// Save Custom Fields
						if ( ! update_post_meta ($post_id, 'email', $email ) ) add_post_meta( $post_id, 'email', $email );
						if ( ! update_post_meta ($post_id, 'password', sha1($password) ) ) add_post_meta( $post_id, 'password', sha1($password) );
						if ( ! update_post_meta ($post_id, 'nombre', $nombre ) ) add_post_meta( $post_id, 'nombre', $nombre );
						if ( ! update_post_meta ($post_id, 'rol', $rol ) ) add_post_meta( $post_id, 'rol', $rol );
						if ( ! update_post_meta ($post_id, 'dependencia', $dependencia ) ) add_post_meta( $post_id, 'dependencia', $dependencia );
						if ( ! update_post_meta ($post_id, 'puesto', $puesto ) ) add_post_meta( $post_id, 'puesto', $puesto );
					}
					
					/*
					//Mail Notification
					$subject = 'Bienvenido a Wordcamp CDMX 2018';
					$template = wp_remote_get( esc_url_raw (get_bloginfo('url').'/mail-register/?param01='.$email.'&param02='.$password) );
					$body = $template['body'];
					$altbody = strip_tags($body);

					//Check Email Data
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Send Email
						add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
						$response = wp_mail( $email, $subject, $body );
					}*/
					
					//Build Response Array
					$array = array(
						'status' => (int)1,
						'msg' => 'success'
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este usuario ya está registrado. Inicia Sesión.',
						'data' => array()
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//editUser
		if ($msg == 'editUser')
		{
			//Read Data
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			$nombre = (isset($fields['nombre'])) ? (string)trim($fields['nombre']) : '';
			$rol = (isset($fields['rol'])) ? (string)trim($fields['rol']) : '';
			$dependencia = (isset($fields['dependencia'])) ? (string)trim($fields['dependencia']) : '';
			$puesto = (isset($fields['puesto'])) ? (string)trim($fields['puesto']) : '';
			
			//Check Values
			if ($email)
			//if ($email && $password)
			{
				//Query User
				$args = array(
					'post_type' => 'usuario',
					'meta_query' => array(
						array(
							'key' => 'email',
							'value' => $email,
							'compare' => '='
						)
					)
				);
				$query = get_posts($args);
				
				//Check User exists
				if (count($query) > 0)
				{
					//Read Object
					
					foreach ($query as $row) { $post_id = $row->ID; }
					
					//Verify
					if ($post_id != 0)
					{
						// Save Custom Fields
						if ( ! update_post_meta ($post_id, 'email', $email ) ) add_post_meta( $post_id, 'email', $email );
						if ($password) { if ( ! update_post_meta ($post_id, 'password', sha1($password) ) ) add_post_meta( $post_id, 'password', sha1($password) ); }
						if ( ! update_post_meta ($post_id, 'nombre', $nombre ) ) add_post_meta( $post_id, 'nombre', $nombre );
						if ( ! update_post_meta ($post_id, 'rol', $rol ) ) add_post_meta( $post_id, 'rol', $rol );
						if ( ! update_post_meta ($post_id, 'dependencia', $dependencia ) ) add_post_meta( $post_id, 'dependencia', $dependencia );
						if ( ! update_post_meta ($post_id, 'puesto', $puesto ) ) add_post_meta( $post_id, 'puesto', $puesto );
					}
					
					/*
					//Mail Notification
					$subject = 'Bienvenido a Wordcamp CDMX 2018';
					$template = wp_remote_get( esc_url_raw (get_bloginfo('url').'/mail-register/?param01='.$email.'&param02='.$password) );
					$body = $template['body'];
					$altbody = strip_tags($body);

					//Check Email Data
					if ($subject != '' && $body != '' && $altbody != '' && $email != '')
					{
						//Send Email
						add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
						$response = wp_mail( $email, $subject, $body );
					}*/
					
					//Build Response Array
					$array = array(
						'status' => (int)1,
						'msg' => 'success'
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'Este usuario ya está registrado. Inicia Sesión.',
						'data' => array()
					);

					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//doLogin
		if ($msg == 'doLogin')
		{
			//Read Data
			$email = (isset($fields['email'])) ? (string)trim($fields['email']) : '';
			$password = (isset($fields['password'])) ? (string)trim($fields['password']) : '';
			
			//Check Values
			if ($email && $password)
			{
				//Query User
				$args = array(
					'post_type' => 'usuario',
					'meta_query' => array(
						'relation' => 'AND',
						array(
							'key' => 'email',
							'value' => $email,
							'compare' => '='
						)
					)
				);
				$query = new WP_Query( $args );
				
				//Check User exists
				if (count($query->posts) > 0)
				{
					//Read Object
					foreach ($query->posts as $row) { $post_id = $row->ID; }
					
					//Verificamos si el Password coincide
					if (sha1($password) == get_field("password", $post_id))
					{
						//Create User Session
						$array_user = array(
							'id' => $post_id,
							'email' => get_post_meta($post_id,'email',true),
							'nombre' => get_post_meta($post_id,'nombre',true),
							'rol' => get_post_meta($post_id,'rol',true),
							'dependencia' => get_post_meta($post_id,'dependencia',true),
							'puesto' => get_post_meta($post_id,'puesto',true),
							'avatar' => get_field("avatar", $post_id)
						);
						
						//Set User Session
						$_SESSION['user'] = $array_user;
						$_SESSION['logged'] = TRUE;
						
						//Build Response Array
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => $array_user
						);
	
						//Print JSON Array
						printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Show Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'La contraseña es incorrecta. Revísala e intenta de nuevo.',
							'data' => array()
						);
		
						//Print JSON Array
						printJSON($array);
						$output = TRUE;
					}
				}
				else
				{
					//Show Error
					$array = array(
						'status' => (int)0,
						'msg' => (string)'El usuario no existe. Crea una cuenta.',
						'data' => array()
					);
	
					//Print JSON Array
					printJSON($array);
					$output = TRUE;
				}
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan campos.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//createForm
		if ($msg == 'createForm')
		{
			//Read Fields
			$titulo = (isset($fields['titulo'])) ? (string)trim($fields['titulo']) : '';
			$unidad = (isset($fields['unidad'])) ? (string)trim($fields['unidad']) : '';
			$array_usuarios = (isset($fields['usuarios'])) ? $fields['usuarios'] : array();
			$array_regiones = (isset($fields['regiones'])) ? $fields['regiones'] : array();
			$array_publicidad = (isset($fields['publicidad'])) ? $fields['publicidad'] : array();
			
			foreach ($array_usuarios as $row_usuario)
			{
				$values_row = explode('@', $row_usuario);
				$usuarios[] = array(
					'id' => $values_row[1],
					'name' => $values_row[0]
				);
			}
			
			for ($i = 0; $i <= count($array_regiones)-1; $i++)
			{
				$campos[] = array(
					'id' => $i+1,
					'campo' => preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $array_regiones[$i]),
					'publicidad' => preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $array_publicidad[$i])
				);
			}
			
			//Register User
			$my_post = array(
			  'post_title'    => wp_strip_all_tags($titulo . ' - ' . $unidad, true),
			  'post_status'   => 'publish',
			  'post_author'   => 1,
			  'post_type'	  => 'formato'
			);

			// Save Data
			$post_id = wp_insert_post( $my_post );

			//Verify
			if ($post_id != 0)
			{
				// Save Custom Fields
				if ( ! update_post_meta ($post_id, 'titulo', $titulo ) ) add_post_meta( $post_id, 'titulo', $titulo );
				if ( ! update_post_meta ($post_id, 'unidad', $unidad ) ) add_post_meta( $post_id, 'unidad', $unidad );
				if ( ! update_post_meta ($post_id, 'usuarios', json_encode($usuarios) ) ) add_post_meta( $post_id, 'usuarios', json_encode($usuarios) );
				if ( ! update_post_meta ($post_id, 'campos', json_encode($campos) ) ) add_post_meta( $post_id, 'campos', json_encode($campos) );
			}
			
			//Build Response Array
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => array(
					'titulo' => $titulo,
					'unidad' => $unidad,
					'usuarios' => $usuarios,
					'campos' => $campos
				)
			);

			//Print JSON Array
			printJSON($array);
			$output = TRUE;
		}
		
		//updateForm
		if ($msg == 'updateForm')
		{
			//Read Fields
			$post_id = (isset($fields['id'])) ? (string)trim($fields['id']) : '';
			$titulo = (isset($fields['titulo'])) ? (string)trim($fields['titulo']) : '';
			$unidad = (isset($fields['unidad'])) ? (string)trim($fields['unidad']) : '';
			$array_usuarios = (isset($fields['usuarios'])) ? $fields['usuarios'] : array();
			$array_regiones = (isset($fields['regiones'])) ? $fields['regiones'] : array();
			$array_publicidad = (isset($fields['publicidad'])) ? $fields['publicidad'] : array();
			
			//Verificamos el ID
			if ($post_id)
			{
				foreach ($array_usuarios as $row_usuario)
				{
					$values_row = explode('@', $row_usuario);
					$usuarios[] = array(
						'id' => $values_row[1],
						'name' => $values_row[0]
					);
				}
				
				for ($i = 0; $i <= count($array_regiones)-1; $i++)
				{
					$campos[] = array(
						'id' => $i+1,
						'campo' => preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $array_regiones[$i]),
						'publicidad' => preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $array_publicidad[$i])
					);
				}
				
				//Verify
				if ($post_id != 0)
				{
					// Save Custom Fields
					if ( ! update_post_meta ($post_id, 'titulo', $titulo ) ) add_post_meta( $post_id, 'titulo', $titulo );
					if ( ! update_post_meta ($post_id, 'unidad', $unidad ) ) add_post_meta( $post_id, 'unidad', $unidad );
					if ( ! update_post_meta ($post_id, 'usuarios', json_encode($usuarios) ) ) add_post_meta( $post_id, 'usuarios', json_encode($usuarios) );
					if ( ! update_post_meta ($post_id, 'campos', json_encode($campos) ) ) add_post_meta( $post_id, 'campos', json_encode($campos) );
				}
				
				//Build Response Array
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => array(
						'titulo' => $titulo,
						'unidad' => $unidad,
						'usuarios' => $usuarios,
						'campos' => $campos
					)
				);
	
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Falta el ID del Formato'
				);
		
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//createNotification
		if ($msg == 'createNotification')
		{
			//Read Fields
			$formato_id = (isset($fields['formato_id'])) ? (string)trim($fields['formato_id']) : '';
			$usuario_id = (isset($fields['usuario_id'])) ? (string)trim($fields['usuario_id']) : '';
			$titulo = (isset($fields['titulo'])) ? (string)trim($fields['titulo']) : '';
			$mensaje = (isset($fields['mensaje'])) ? (string)trim($fields['mensaje']) : '';
			
			//Check Data
			if ($formato_id && $usuario_id)
			{
				//Register Notification
				$usuario = get_post($usuario_id);
				$my_post = array(
				  'post_title'    => wp_strip_all_tags($titulo . ' - ' . get_field("email", $usuario->ID), true),
				  'post_status'   => 'publish',
				  'post_author'   => 1,
				  'post_type'	  => 'notificacion'
				);
	
				// Save Data
				$post_id = wp_insert_post( $my_post );
	
				//Verify
				if ($post_id != 0)
				{
					// Save Custom Fields
					if ( ! update_post_meta ($post_id, 'titulo', $titulo ) ) add_post_meta( $post_id, 'titulo', $titulo );
					if ( ! update_post_meta ($post_id, 'mensaje', $mensaje ) ) add_post_meta( $post_id, 'mensaje', $mensaje );
					if ( ! update_post_meta ($post_id, 'formato', $formato_id ) ) add_post_meta( $post_id, 'formato', $formato_id );
					if ( ! update_post_meta ($post_id, 'usuario', $usuario_id ) ) add_post_meta( $post_id, 'usuario', $usuario_id );
				}
				
				//Build Response Array
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => array(
						'id' => $post_id,
						'titulo' => $titulo
					)
				);
	
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Error al enviar. Intenta más tarde.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//approveData
		if ($msg == 'approveData')
		{
			//Read Fields
			$formato_id = (isset($fields['formato_id'])) ? (string)trim($fields['formato_id']) : '';
			$usuario_id = (isset($fields['usuario_id'])) ? (string)trim($fields['usuario_id']) : '';
			$post_id = (isset($fields['post_id'])) ? (string)trim($fields['post_id']) : '';
			
			//Check Data
			if ($formato_id && $usuario_id && $post_id)
			{
				//Update Status
				if ( ! update_post_meta ($post_id, 'aprobado', 1 ) ) add_post_meta( $post_id, 'aprobado', 1 );
				
				//Register Notification
				$titulo = 'Datos aprobados del formulario "' . get_field( "titulo", $formato_id ) . '"';
				$mensaje = 'Se aprobaron los datos del formulario "' . get_field( "titulo", $formato_id ) . '" por el usuario "' . get_field( "nombre", $usuario_id ) .'"';
				$my_post = array(
				  'post_title'    => wp_strip_all_tags($titulo . ' - ' . get_field("email", $usuario_id), true),
				  'post_status'   => 'publish',
				  'post_author'   => 1,
				  'post_type'	  => 'notificacion'
				);
	
				// Save Data
				$post_id = wp_insert_post( $my_post );
	
				//Verify
				if ($post_id != 0)
				{
					// Save Custom Fields
					if ( ! update_post_meta ($post_id, 'titulo', $titulo ) ) add_post_meta( $post_id, 'titulo', $titulo );
					if ( ! update_post_meta ($post_id, 'mensaje', $mensaje ) ) add_post_meta( $post_id, 'mensaje', $mensaje );
					if ( ! update_post_meta ($post_id, 'formato', $formato_id ) ) add_post_meta( $post_id, 'formato', $formato_id );
					if ( ! update_post_meta ($post_id, 'usuario', $usuario_id ) ) add_post_meta( $post_id, 'usuario', $usuario_id );
				}
				
				//Build Response Array
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => array(
						'id' => $post_id,
						'titulo' => $titulo
					)
				);
	
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Error al aprobar. Intenta más tarde.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//updateDataForm
		if ($msg == 'updateDataForm')
		{
			//Read Fields
			$post_id = (isset($fields['post_id'])) ? (string)trim($fields['post_id']) : '';
			$datos_json = (isset($fields['datos'])) ? $fields['datos'] : '';
			
			//Check Data
			if ($post_id)
			{
				//Update Status
				$data = json_encode($datos_json);
				if ( ! update_post_meta ($post_id, 'datos', $data ) ) add_post_meta( $post_id, 'datos', $data );
			
				//Build Response Array
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $data
				);
	
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Error al aprobar. Intenta más tarde.',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//FISCALIA PARTE PUBLICA
		
		//getForms
		if ($msg == 'getForms')
		{
			//Query Forms
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'posts_per_page'   => 10,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'formato',
				'post_status'      => 'publish',
				'paged'			   => $paged,
				'suppress_filters' => false 
			);
			$query = new WP_Query( $args );
			$data = array();
			
			//Check Query
			if (count($query->posts) > 0)
			{
				//Process Forms
				foreach ($query->posts as $row)
				{
					$data[] = array(
						'id' => $row->ID,
						'titulo' => get_field("titulo", $row->ID),
						'unidad' => get_field("unidad", $row->ID),
					);
				}
			}
			
			//Build Response Array
			$array = array(
				'status' => (int)1,
				'msg' => 'success',
				'data' => $data
			);

			//Print JSON Array
			printJSON($array);
			$output = TRUE;
		}
		
		//getForm
		if ($msg == 'getForm')
		{
			//Read Values
			$id = (isset($fields['id'])) ? (string)trim($fields['id']) : '';
			
			//Check ID
			if ($id)
			{
				//Query Forms
				$row = get_post($id);
				$data = array();
				
				//Process Forms
				$data = array(
					'id' => $row->ID,
					'titulo' => get_field("titulo", $row->ID),
					'unidad' => get_field("unidad", $row->ID),
				);
								
				//Build Response Array
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $data
				);
	
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//getFormFieldsData
		if ($msg == 'getFormFieldsData')
		{
			//Read Values
			$id = (isset($fields['form_id'])) ? (string)trim($fields['form_id']) : '';
			
			//Check ID
			if ($id)
			{
				//Query Forms
				$row = get_post($id);
				$data = array();
				
				//Query Data
				$args = array(
					'posts_per_page'   => -1,
					'orderby'          => 'date',
					'order'            => 'DESC',
					'post_type'        => 'dato',
					'post_status'      => 'publish',
					'suppress_filters' => false,
					'meta_query' => array(
						array(
							'key' => 'formato',
							'value' => $row->ID,
							'compare' => '='
						)
					)
				);
				$query_datos = new WP_Query( $args );
				$datos = array(); $contador_datos = 0;
				
				$html_datos = '';
				$html_datos.= '<div class="container-fluid no-gutters">';
				$html_datos.= '		<div class="row">';
				$html_datos.= '			<div class="col">';
				$html_datos.= '				<div class="row marRig5">';
				$html_datos.= '					<div class="table-responsive">';
				$html_datos.= '						<table class="table">';
				$html_datos.= '							<thead class="ms-medium font20">';
				$html_datos.= '								<tr>';
				$html_datos.= '									<th scope="" class="bdrTopNone">Filas</th>';
				$html_datos.= '								</tr>';
				$html_datos.= '							</thead>';
				$html_datos.= '							<tbody class="ms-light font14">';
				
				while ( $query_datos->have_posts() ) : $query_datos->the_post(); setup_postdata( $post );
					
					$contador_datos++;
					
					$datos[] = array(
						'id' => $post->ID,
						'titulo' => get_field("titulo", $post->ID),
						'fecha' => get_field("fecha", $post->ID),
						'datos' => get_field("datos", $post->ID),
						'aprobado' => get_field("aprobado", $post->ID)
					);
					
					$html_datos.= '							<tr>';
					$html_datos.= '								<td class="tdLeftedit">';
					$html_datos.= '									<div class="input-group">';
					$html_datos.= '										<span class="padTabLeftSoc" id="filas">';
					$html_datos.= '											'.get_field("titulo", $post->ID);
					$html_datos.= '										</span>';
					$html_datos.= '										<i class="fas fa-times c-neon equix" rel="filas"></i>';
					$html_datos.= '									</div>';
					$html_datos.= '								</td>';
					$html_datos.= '							</tr>';
					
				endwhile; wp_reset_postdata();
				
				$html_datos.= '							</tbody>';
				$html_datos.= '						</table>';
				$html_datos.= '					</div>';
				$html_datos.= '				</div>';
				$html_datos.= '			</div>';
				$html_datos.= '		</div>';
				$html_datos.= '</div>';
				
				$campos = get_field("campos", $row->ID);
				$campos_data = json_decode($campos);
				$html_campos = '';
				$html_campos.= '<div class="container-fluid no-gutters">';
				$html_campos.= '	<div class="row">';
				$html_campos.= '		<div class="col">';
				$html_campos.= '			<div class="row marRig5">';
				$html_campos.= '				<div class="table-responsive">';
				$html_campos.= '					<table class="table">';
				$html_campos.= '						<thead class="ms-medium font20">';
				$html_campos.= '							<tr>';
				$html_campos.= '								<th scope="" class="bdrTopNone">Columnas</th>';
				$html_campos.= '							</tr>';
				$html_campos.= '						</thead>';
				$html_campos.= '						<tbody class="ms-light font14">';
				
				foreach ($campos_data as $campo)
				{
					//Revisamos
					if ($campo->publicidad == '1')
					{
						$html_campos.= '							<tr>';
						$html_campos.= '								<td class="tdLeftedit">';
						$html_campos.= '									<div class="input-group">';
						$html_campos.= '										<span class="padTabLeftSoc" id="columnas">';
						$html_campos.= '											'.preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $campo->campo);
						$html_campos.= '										</span>';
						$html_campos.= '										<i class="fas fa-times c-neon equix borrarColumna" rel="columnas"></i>';
						$html_campos.= '									</div>';
						$html_campos.= '								</td>';
						$html_campos.= '							</tr>';
					}
				}
				
				$html_campos.= '						</tbody>';
				$html_campos.= '					</table>';
				$html_campos.= '				</div>';
				$html_campos.= '			</div>';
				$html_campos.= '		</div>';
				$html_campos.= '	</div>';
				$html_campos.= '</div>';
				
				//Process Forms
				$data = array(
					'id' => $row->ID,
					'titulo' => get_field("titulo", $row->ID),
					'unidad' => get_field("unidad", $row->ID),
					'campos' => $campos,
					'datos' => $datos,
					'html_campos' => $html_campos,
					'html_datos' => $html_datos
				);
								
				//Build Response Array
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $data
				);
	
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
		
		//getFormData
		if ($msg == 'getFormData')
		{
			//Read Values
			$id = (isset($fields['form_id'])) ? (string)trim($fields['form_id']) : '';
			
			//Check ID
			if ($id)
			{
				//Query Forms
				$row = get_post($id);
				$data = array();
				
				//Query Data
				$args = array(
					'posts_per_page'   => -1,
					'orderby'          => 'date',
					'order'            => 'ASC',
					'post_type'        => 'dato',
					'post_status'      => 'publish',
					'suppress_filters' => false,
					'meta_query' => array(
						array(
							'key' => 'formato',
							'value' => $row->ID,
							'compare' => '='
						)
					)
				);
				$query_datos = new WP_Query( $args );
				$datos = array(); $contador_datos = 0;
				
				while ( $query_datos->have_posts() ) : $query_datos->the_post(); setup_postdata( $post );
					
					$contador_datos++;
					
					$datos[] = array(
						'id' => $post->ID,
						'titulo' => get_field("titulo", $post->ID),
						'fecha' => get_field("fecha", $post->ID),
						'datos' => get_field("datos", $post->ID),
						'aprobado' => get_field("aprobado", $post->ID)
					);
					
				endwhile; wp_reset_postdata();
				
				$campos = get_field("campos", $row->ID);
				$campos_data = json_decode($campos);
				
				//Process Forms
				$data = array(
					'id' => $row->ID,
					'titulo' => get_field("titulo", $row->ID),
					'unidad' => get_field("unidad", $row->ID),
					'campos' => $campos,
					'datos' => $datos
				);
								
				//Build Response Array
				$array = array(
					'status' => (int)1,
					'msg' => 'success',
					'data' => $data
				);
	
				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
			else
			{
				//Show Error
				$array = array(
					'status' => (int)0,
					'msg' => (string)'Faltan Campos',
					'data' => array()
				);

				//Print JSON Array
				printJSON($array);
				$output = TRUE;
			}
		}
	}
	else
	{
		//Show Error
		$array = array(
			'status' => (int)0,
			'msg' => (string)'API Call Invalid.'
		);

		//Print JSON Array
		printJSON($array);
		$output = TRUE;
	}

	//Check Output
	if (!$output)
	{
		//Show Error
		$array = array(
			'status' => (int)0,
			'msg' => (string)'API Error.'
		);

		//Print JSON Array
		printJSON($array);
		$output = TRUE;
	}
	
?>