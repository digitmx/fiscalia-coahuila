<?php /* Template Name: Informacion Estadistica Rol 3 */ ?>
<?php if (!isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/logout/' ); } ?>
<?php if ($_SESSION['user']['rol'] != 'rol-3') { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia-rol3"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						INFORMACIÓN ESTADISTICA
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<?php
			//Query News
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'posts_per_page'   => 10,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'formato',
				'post_status'      => 'publish',
				'paged'			   => $paged,
				'suppress_filters' => false 
			);
			$query = new WP_Query( $args );
		?>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-tabla-fiscalia-coahuila marTop40 marBot40">
						<table class="table table-borderless table-striped">
							<tbody>
								<?php while ( $query->have_posts() ) : $query->the_post(); setup_postdata( $post ); ?>
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase"><?php echo get_field("titulo", $post->ID); ?></span> <a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/orden/?id=<?php echo $post->ID; ?>" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								<?php endwhile; wp_reset_postdata(); ?>
								<!--
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase">INFORME DE MANDAMIENTOS JUDICIALES</span> <a href="<?php bloginfo("url"); ?>/rol-3/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase">ÓRDENES DE APREHENSIÓN VIGENTES PENDIENTES DE CUMPLIMENTAR  </span> <a href="<?php bloginfo("url"); ?>/rol-3/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase">ÓRDENES DE APREHENSIÓN VIGENTES PENDIENTES DE CUMPLIMENTAR POR MUNICIPIO</span> <a href="<?php bloginfo("url"); ?>/rol-3/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase">ÓRDENES DE APREHENSIÓN VIGENTES POR REGIÓN Y DELITO</span> <a href="<?php bloginfo("url"); ?>/rol-3/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase">REGISTRO DE EXTRADICIONES</span> <a href="<?php bloginfo("url"); ?>/rol-3/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase">REGISTRO DE ASISTENCIAS JURIDICAS</span> <a href="<?php bloginfo("url"); ?>/rol-3/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase">DEPORTACIONES CONTROLADAS</span> <a href="<?php bloginfo("url"); ?>/rol-3/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase">SOBRESEIMIENTOS POR REPARACION DEL DAÑO</span> <a href="<?php bloginfo("url"); ?>/rol-3/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase">RELACION DE PERSONAS INGRESADAS AL CERESO.  SEMANA # DEL # AL # DE # DE #</span> <a href="<?php bloginfo("url"); ?>/rol-3/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								<tr>
									<th scope="row">
										<span class="ms-regular font12 uppercase">ASUNTOS EN LOS QUE LA DIRECCIÓN GENERAL DE CONTROL DE PROCESOS HA DADO…</span> <a href="<?php bloginfo("url"); ?>/rol-3/informacion-estadistica/orden/" class="float-right ms-bold font11 c-green uppercase padTop5">VER  INFORMACIÓN ESTADISTICA</a>
									</th>
								</tr>
								-->
							</tbody>
						</table>
						<!--
						<div class="col text-center border-line">
							<div class="bloque-tabla">
								<form class="form-inline ms-medium font11">
									<label class="my-1 mr-2" for="listaUsuarios">Articulos por página:</label>
									<select class="custom-select my-1 mr-sm-2 btEditListUser" id="listaUsuarios">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option selected  value="10">10</option>
									</select>
									<div class="col">
										<span class="">2 - 10 de 30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
										<a href="#"><i class="fas fa-chevron-left c-green font14"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="#"><i class="fas fa-chevron-right font14 c-green"></i></a>
									</div>
								</form>
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>