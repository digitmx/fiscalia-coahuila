<?php /* Template Name: Subir Informacion Estadistica Rol 4 */ ?>
<?php $id = (isset($_GET['id'])) ? (string)trim($_GET['id']) : ''; ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia-rol4"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/rol-4/informacion-estadistica/orden/?id=<?php echo $id; ?>" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						Subir Información Estadística
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="container marTop30 marBot30">
				<div class="row justify-content-center">
					<div class="col-sm-12 col-md-10 col-lg-6">
						<div class="contenedor-texto-perfil marTop40">
							<form id="formUploadCSV" name="formUploadCSV" accept-charset="utf-8" enctype="multipart/form-data" method="post" action="<?php bloginfo("url"); ?>/rol-4/informacion-estadistica/process-csv/" class="row boxFormPerfil-coa">
								<input type="hidden" id="idform" name="idform" value="<?php echo $id; ?>" />
								<div class="col-12 text-white marTop10">
									<span class="ms-medium font10 c-green marLef20">
										Titulo
									</span>
									<div class="input-group marTop10">
										<input type="text" id="titulo_csv" name="titulo_csv" class="form-control ms-light font14 editListaUsuario" placeholder="Nombre">
									</div>
								</div>
								<div class="col-12 text-white marTop10 marBot40">
									<span class="ms-medium font10 c-green marLef20">
										Fecha
									</span>
									<div class="input-group marTop10">
										<input type="date" id="fecha_csv" name="fecha_csv" class="form-control ms-light font14 editListaUsuario" placeholder="00/00/00">
									</div>
								</div>
								<div class="col">
									<!-- Modal Subir CSV -->
									<div class="modal fade" id="ModalSubir" tabindex="-1" role="dialog" aria-labelledby="ModalSubirTitle" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header justify-content-center">
													<h5 class="modal-title ms-semibold font16" id="ModalSubirTitle">Subir Documento</h5>
												</div>
												<div class="modal-body">
													<div class="container-fluid body-Subir">
														<div class="container body-aprobar-guardar">
															<p class="ms-light font24 text-center pos-Notificacion">
																Tu CSV fue subido con éxito.
															</p>
														</div>
													</div>
												</div>
												<div class="modal-footer justify-content-center">
													<div class="col-6 footer-subir-aceptar">
														<div class="text-center">
															<button id="aceptar-apr" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" data-dismiss="modal">
															Aceptar
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="text-center marBot20">
										<input type="file" id="csvFile" name="csvFile" style="display: contents; visibility: hidden;" />
										<!--<button id="" type="button" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas" data-toggle="modal" data-target="#ModalSubir">-->
										<button id="btnUploadCSV" name="btnUploadCSV" type="button" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnVerMas">
										Subir CSV
										</button>
									</div>
								</div>
							</form>
							<div class="row btnGuardarEditar">
								<div class="col-6">
									<div class="text-center">
										<a href="<?php bloginfo("url"); ?>/rol-4/informacion-estadistica/orden/?id=<?php echo $id; ?>" class="btn btn-lg btn-block ms-bold font11 btEditGuardar">
										Cancelar
										</a>
									</div>
								</div>
								<div class="col-6">
									<div class="text-center">
										<button id="btnSaveCSV" name="btnSaveCSV" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar" rel="1" >
										Guardar
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>