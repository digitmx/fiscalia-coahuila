<?php /* Template Name: Procesar Informacion Estadistica Rol 4 */ ?>
<?php
	//Leer Datos
	$id = (isset($_POST['idform'])) ? (string)trim($_POST['idform']) : '';
	$titulo = (isset($_POST['titulo_csv'])) ? (string)trim($_POST['titulo_csv']) : '';
	$fecha = (isset($_POST['fecha_csv'])) ? (string)trim($_POST['fecha_csv']) : '';
	$csv_data = array();
	
	//Procesamos el CSV
	if ($_FILES['csvFile']['name'])
	{
		$filename = explode('.', $_FILES['csvFile']['name']);
		if ($filename[1] == 'csv')
		{
			$handle = fopen($_FILES['csvFile']['tmp_name'], "r");
			while (( $data = fgetcsv ( $handle , 20000 , ',')) !== FALSE ) 
			{
				$csv_data[] = $data;
			}
			
			fclose($handle);
		}
	}
	
	//Register User
	$my_post = array(
		'post_title'    => wp_strip_all_tags($titulo . ' - ' . $fecha, true),
		'post_status'   => 'publish',
		'post_author'   => 1,
		'post_type'	  => 'dato'
	);

	// Save Data
	$post_id = wp_insert_post( $my_post );

	//Verify
	if ($post_id != 0)
	{
		// Save Custom Fields
		if ( ! update_post_meta ($post_id, 'titulo', $titulo ) ) add_post_meta( $post_id, 'titulo', $titulo );
		if ( ! update_post_meta ($post_id, 'fecha', $fecha ) ) add_post_meta( $post_id, 'fecha', $fecha );
		if ( ! update_post_meta ($post_id, 'formato', $id ) ) add_post_meta( $post_id, 'formato', $id );
		if ( ! update_post_meta ($post_id, 'datos', json_encode($csv_data) ) ) add_post_meta( $post_id, 'datos', json_encode($csv_data) );
	}
	
	wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/informacion-estadistica/orden/?id=' . $id );
	
	/*
	echo '<pre>';
	print_r($csv_data);
	echo '</pre>';
	*/
?>