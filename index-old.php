<?php get_header(); ?>

	<div class="container-fluid bgIndex">
		<div class="container indexSom">
			<div class="row sombraInd justify-content-center">
				<div class="col-sm-12 col-md-11 col-lg-8 boxShadow">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<span class="centered c-white ms-extrabold font30 block">FISCALIA COAHUILA</span>
								<img src="<?php bloginfo("template_directory"); ?>/img/logo-fiscalia.png" class="img-fluid mx-auto d-block" alt="Responsive image">
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-2">
								<div class="centered">
									<img src="<?php bloginfo("template_directory"); ?>/img/icono--perfilbackend.svg" class="img-fluid mx-auto text-center padTop80" alt="Responsive image">
								</div>
							</div>
							<div class="col-6 col-sm-5 text-white">
								<div class="text-center padTop80">
									<button id="home-rol1" type="submit" class="btn btn-lg btn-block ms-extrabold font13 btnIndex">
										<span class="float-left"><?php _e("Rol 1","faco_v1"); ?></span>
										<img src="<?php bloginfo("template_directory"); ?>/img/icono--entrar.svg" class="img-fluid mx-auto float-right" alt="Responsive image">
									</button>
								</div>
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col-2">
								<div class="centered">
									<img src="<?php bloginfo("template_directory"); ?>/img/icono--perfilbackend.svg" class="img-fluid mx-auto text-center padTop40" alt="Responsive image">
								</div>
							</div>
							<div class="col-6 col-sm-5 text-white">
								<div class="text-center padTop40">
									<button id="home-rol2" type="submit" class="btn btn-lg btn-block ms-extrabold font13 btnIndex">
										<span class="float-left"><?php _e("Rol 2","faco_v1"); ?></span>
										<img src="<?php bloginfo("template_directory"); ?>/img/icono--entrar.svg" class="img-fluid mx-auto float-right" alt="Responsive image">
									</button>
								</div>
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col-2">
								<div class="centered">
									<img src="<?php bloginfo("template_directory"); ?>/img/icono--perfilbackend.svg" class="img-fluid mx-auto text-center padTop40" alt="Responsive image">
								</div>
							</div>
							<div class="col-6 col-sm-5 text-white">
								<div class="text-center padTop40">
									<button id="home-rol3" type="submit" class="btn btn-lg btn-block ms-extrabold font13 btnIndex">
										<span class="float-left"><?php _e("Rol 3","faco_v1"); ?></span>
										<img src="<?php bloginfo("template_directory"); ?>/img/icono--entrar.svg" class="img-fluid mx-auto float-right" alt="Responsive image">
									</button>
								</div>
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col-2">
								<div class="centered">
									<img src="<?php bloginfo("template_directory"); ?>/img/icono--perfilbackend.svg" class="img-fluid mx-auto text-center padTop40" alt="Responsive image">
								</div>
							</div>
							<div class="col-6 col-sm-5 text-white">
								<div class="text-center padTop40">
									<button id="home-rol4" type="submit" class="btn btn-lg btn-block ms-extrabold font13 btnIndex">
										<span class="float-left"><?php _e("Rol 4","faco_v1"); ?></span>
										<img src="<?php bloginfo("template_directory"); ?>/img/icono--entrar.svg" class="img-fluid mx-auto float-right" alt="Responsive image">
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php //get_footer(); ?>