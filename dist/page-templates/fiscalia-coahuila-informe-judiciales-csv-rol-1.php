<?php /* Template Name: Informe Mandamientos Judiciales CSV Rol 1 */ ?>
<?php if (!isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/logout/' ); } ?>
<?php if ($_SESSION['user']['rol'] != 'rol-1') { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php $id = (isset($_GET['id'])) ? (string)trim($_GET['id']) : ''; ?>
<?php if ($id) { $datos = get_post($id); } else { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php $formato = get_post(get_field("formato", $datos->ID)); ?>
<?php $json = get_field("datos", $datos->ID); $json_decode = json_decode($json); ?>
<?php $tabla_header = $json_decode[0]; $contador_json = 0; ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/orden/?id=<?php echo $formato->ID; ?>" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						<?php echo get_field("titulo", $id); ?>
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-tabla-fiscalia-coahuila-csv">
						<table class="table table-responsive">
							<thead>
								<tr>
									<?php foreach ($tabla_header as $header) { ?>
									<th scope="col" class="ms-semibold font13 uppercase edit-title-tabla-csv"><?php echo preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $header); ?></th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($json_decode as $row) { $contador_json++; ?>
									<?php if ($contador_json > 1) { ?>
									<tr>
										<?php for ($i = 0; $i < count($row); $i++) { ?>
										<td class="ms-regular font12 edit-tabla-csv"><?php echo preg_replace('/u([\da-fA-F]{4})/', '&#x\1;', $row[$i]); ?></td>
										<?php } ?>
									</tr>
									<?php } ?>
								<?php } ?>
								<!--
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">2</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">3</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">4</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">5</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">6</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">7</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								-->
							</tbody>
						</table>
						<div class="row justify-content-center btnGuardarEditar">
							<!-- Modal -->
							<div class="modal fade" id="ModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											...
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-primary">Save changes</button>
										</div>
									</div>
								</div>
							</div>
							<div class="col col-lg-10">
								<div class="row">
									<div class="col-6 col-lg-3">
										<div class="text-center">
											<button id="btNotificar" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" data-toggle="modal" data-target="#ModalNotificar">
											NOTIFICAR
											</button>
											<!-- Modal Notificacion -->
											<div class="modal fade" id="ModalNotificar" tabindex="-1" role="dialog" aria-labelledby="ModalNotificarTitle" aria-hidden="true">
												<div class="modal-dialog modal-dialog-centered" role="document">
													<div class="modal-content">
														<div class="modal-header justify-content-center">
															<h5 class="modal-title ms-semibold font16" id="ModalNotificarTitle">NOTIFICACIÓN: <?php echo get_field("titulo", $formato->ID); ?></h5>
														</div>
														<div class="modal-body">
															<div class="container-fluid body-notificacion">
																<div class="row">
																	<div class="col text-white">
																		<span class="ms-medium font10 float-left c-green marLef20">
																			Titulo
																		</span>
																		<div class="input-group marTop10">
																			<input id="notificacionTitulo" name="notificacionTitulo" type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Titulo">
																		</div>
																	</div>
																</div>
																<form class="marTop20">
																	<div class="form-group">
																		<textarea id="notificacionMensaje" name="notificacionMensaje" class="form-control editListaUsuario" id="" rows="10"></textarea>
																	</div>
																</form>
															</div>
															<div class="container body-notificacion-aceptar">
																<p class="ms-light font24 text-center pos-Notificacion" id="notificacionResultado"></p>
															</div>
														</div>
														<div class="modal-footer justify-content-center">
															<div class="col footer-notificacion">
																<div class="text-center">
																	<input type="hidden" id="formato_id" name="formato_id" value="<?php echo $formato->ID; ?>" />
																	<input type="hidden" id="usuario_id" name="usuario_id" value="<?php echo $_SESSION['user']['id']; ?>" />
																	<button id="enviarNotificacion" name="enviarNotificacion" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv">
																		ENVIAR NOTIFICACIÓN
																	</button>
																</div>
															</div>
															<div class="col footer-notificacion">
																<div class="text-center">
																	<button id="cerrar-not" type="button" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" data-dismiss="modal">
																	Cancelar
																	</button>
																</div>
															</div>
															<div class="col-6 footer-notificacion-aceptar">
																<div class="text-center">
																	<button id="aceptar-not" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" data-dismiss="modal">
																	Aceptar
																	</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-6 col-lg-3">
										<div class="text-center">
											<button id="btPublicar" <?=(get_field("aprobado",$id)) ? ' disabled="" ' : '';?> type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" data-toggle="modal" data-target="#ModalAprobar">
												APROBAR PUBLICACIÓN
											</button>
											<!-- Modal Notificacion -->
											<div class="modal fade" id="ModalAprobar" tabindex="-1" role="dialog" aria-labelledby="ModalAprobarTitle" aria-hidden="true">
												<div class="modal-dialog modal-dialog-centered" role="document">
													<div class="modal-content">
														<div class="modal-header justify-content-center">
															<h5 class="modal-title ms-semibold font16" id="ModalAprobarTitle"><?php echo get_field("titulo", $formato->ID); ?></h5>
														</div>
														<div class="modal-body">
															<div class="container-fluid body-aprobar">
																<div class="container body-aprobar-guardar">
																	<p class="ms-light font24 text-center pos-Notificacion">
																		¿Esta seguro de aprobar la publicación?
																	</p>
																</div>
															</div>
															<div class="container body-aprobar-aceptar">
																<p class="ms-light font24 text-center pos-Notificacion" id="aprobacionResultado"></p>
															</div>
														</div>
														<div class="modal-footer justify-content-center">
															<div class="col footer-aprobar">
																<div class="text-center">
																	<input type="hidden" id="formato_id" name="formato_id" value="<?php echo $formato->ID; ?>" />
																	<input type="hidden" id="usuario_id" name="usuario_id" value="<?php echo $_SESSION['user']['id']; ?>" />
																	<input type="hidden" id="post_id" name="post_id" value="<?php echo $id; ?>" />
																	<button id="guardarAprobacion" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv">
																		Guardar
																	</button>
																</div>
															</div>
															<div class="col footer-aprobar">
																<div class="text-center">
																	<button id="cerrar-apr" type="button" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" data-dismiss="modal">
																	Cancelar
																	</button>
																</div>
															</div>
															<div class="col-6 footer-aprobar-aceptar">
																<div class="text-center">
																	<button id="aceptar-apr" type="button" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" data-dismiss="modal">
																	Aceptar
																	</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-6 col-lg-3">
										<div class="text-center">
											<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/orden/editar-tabla/?id=<?php echo $id; ?>">
												<button id="btEditar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" rel="1" >
												Editar
												</button>
											</a>
										</div>
									</div>
									<div class="col-6 col-lg-3">
										<div class="text-center">
											<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/orden/?id=<?php echo $formato->ID; ?>" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv">
												Cancelar
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>