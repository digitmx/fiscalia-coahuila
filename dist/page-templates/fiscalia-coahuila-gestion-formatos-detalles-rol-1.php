<?php /* Template Name: Gestión Formatos Detalles Rol 1 */ ?>

<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="javascript: history.go(-1)" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						INFORME DE MANDAMIENTOS JUDICIALES
						<div class="col-2 no-padding p-absolute btn-CrearUsuario">
							<div class="text-center padTop10">
								<button id="" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userNotificaciones" class="btn ms-bold font11 c-green btnUser" rel="1">
								Crear Nuevo Formato
								</button>
							</div>
						</div>
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-tabla-fiscalia-coahuila">
						<div class="container-fluid">
							<div class="row marTop20 marBot20">
								<div class="col-4 text-white">
									<span class="ms-medium font10 c-green marLef20">
										Titulo
									</span>
									<div class="input-group marTop10">
										<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Nombre del titulo">
									</div>
								</div>
								<div class="col-4 text-white">
									<span class="ms-medium font10 c-green marLef20">
										Rol
									</span>
									<div class="input-group marTop10">
										<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Rol 1">
									</div>
								</div>
								<div class="col-4 text-white">
									<span class="ms-medium font10 c-green marLef20">
										Unidad
									</span>
									<div class="input-group marTop10">
										<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Nombre de unidad">
									</div>
								</div>
							</div>
						</div>
						<table class="table table-borderless table-responsive tabla-responsiva">
							<thead>
								<tr>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla padLeft75rem">Titulo</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla padLeft75rem">Publicidad</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Región">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="NÚMERO DE PROCESO O CAUSA...">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="JUZGADO (TRADICIONAL O DEL...">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="DATOS DEL INCULPADO O IMPUTA...">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="DOMICILIO DEL INCULPADO">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="DELITO">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="FECHA DEL MANDAMIENTO...">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="MUNICIPIO DE ORIGEN...">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="MUNICIPIO DE ORIGEN DONDE...">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="FECHA EN QUE SE CUMPLIMENTO...">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="SITUACION JURIDICA (AFP...">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="NUMERO DE CARPETA DE ...">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td scope="row" class="text-center vertical-mid">
										<div class="contenedor-cruz">
											<i class="fas fa-arrows-alt cruz-gestion c-green font20"></i>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="SEÑALAR NOMBRE DE LA OCCISO...">
											</fieldset>
										</div>
									</td>
									<td class="ms-light font14">
										<div class="input-group marTop10 inline">
											<fieldset disabled>
												<input type="text" class="form-control ms-light font14 editListaUsuario" placeholder="Publicidad">
											</fieldset>
										</div>
									</td>
									<td>
										<div class="col">
											<div class="text-center">
												<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
												Eliminar
												</button>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="col text-center border-line">
							<div class="bloque-tabla">
								<div class="col">
									<div class="text-center">
										<button id="btCancelar" type="submit" class="btn btn-lg btn-block ms-bold font11 btnVerMas" rel="1" >
										AGREGAR COLUMNA
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row justify-content-center btnGuardarEditar">
							<div class="col col-lg-10">
								<div class="row justify-content-center">
									<div class="col-6 col-lg-3">
										<div class="text-center">
											<button id="btDesactivar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv" rel="1" >
											DESACTIVAR
											</button>
										</div>
									</div>
									<div class="col-6 col-lg-3">
										<div class="text-center">
											<button id="btGuardar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" rel="1" >
											GUARDAR 
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>