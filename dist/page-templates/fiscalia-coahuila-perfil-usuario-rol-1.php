<?php /* Template Name: Fiscalia Coahuila Editar Perfil Usuario Rol 1 */ ?>
<?php if (!isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/logout/' ); } ?>
<?php if ($_SESSION['user']['rol'] != 'rol-1') { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php $post_id = (isset($_GET['id'])) ? (string)trim($_GET['id']) : ''; ?>
<?php if ($post_id) { $usuario = get_post($post_id); } else { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/usuarios/" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						Editar Usuario
						<div class="col-2 no-padding p-absolute btn-CrearUsuario">
							<div class="text-center padTop10">
								<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/crear-usuario/" class="btn ms-bold font11 c-green btnUser">
									Crear Usuario
								</a>
							</div>
						</div>
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="container marTop30 marBot30">
				<div class="row justify-content-center">
					<div class="col-sm-12 col-md-10 col-lg-6">
						<div class="contenedor-texto-perfil marTop40">
							<form id="edit_form" name="edit_form">
								<div class="row boxFormPerfil-coa">
									<div class="col-12 text-white marTop40">
										<span class="ms-medium font10 c-green marLef20">
											Nombre
										</span>
										<div class="input-group marTop10">
											<input type="text" id="inputName" name="inputName" class="form-control ms-light font14 editListaUsuario" placeholder="Escribe el nombre completo" value="<?php the_field("nombre", $usuario->ID); ?>">
										</div>
									</div>
									<div class="col-12 text-white marTop10">
										<span class="ms-medium font10 c-green marLef20">
											Rol
										</span>
										<div class="input-group marTop10">
											<div class="input-field col s12 no-padding">
												<?php $rol = get_field("rol", $usuario->ID); ?>
												<select id="inputRol" name="inputRol" class="form-control ms-light font14 editListaUsuario">
													<option value="rol-2"<?=($rol == 'rol-2') ? ' selected' : ''; ?>>Gestor 2</option>
													<option value="rol-3"<?=($rol == 'rol-3') ? ' selected' : ''; ?>>Gestor 3</option>
													<option value="rol-4"<?=($rol == 'rol-4') ? ' selected' : ''; ?>>Gestor 4</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-12 text-white marTop20">
										<span class="ms-medium font10 c-green marLef20">
											Correo Electrónico
										</span>
										<div class="input-group marTop10">
											<input disabled type="email" id="inputEmail" name="inputEmail" class="form-control ms-light font14 editListaUsuario" value="<?php the_field("email", $usuario->ID); ?>">
										</div>
									</div>
									<div class="col-12 text-white marTop20">
										<span class="ms-medium font10 c-green marLef20">
											Contraseña
										</span>
										<div class="input-group marTop10">
											<input type="password" id="inputPassword" name="inputPassword" class="form-control ms-light font14 editListaUsuario" placeholder="Escribe un nuevo password para actualizarlo">
										</div>
									</div>
									<div class="col-12 text-white marTop10">
										<span class="ms-medium font10 c-green marLef20">
											Dependencía
										</span>
										<div class="input-group marTop10">
											<input type="text" id="inputDependencia" name="inputDependencia" class="form-control ms-light font14 editListaUsuario" placeholder="Privada" value="<?php the_field("dependencia", $usuario->ID); ?>">
										</div>
									</div>
									<div class="col-12 text-white marTop10 marBot40">
										<span class="ms-medium font10 c-green marLef20">
											Puesto
										</span>
										<div class="input-group marTop10">
											<input type="text" id="inputPuesto" name="inputPuesto" class="form-control ms-light font14 editListaUsuario" placeholder="Director" value="<?php the_field("puesto", $usuario->ID); ?>">
										</div>
									</div>
								</div>
								<div class="row btnGuardarEditar">
									<div class="col-6">
										<div class="text-center">
											<button id="btnGuardar" name="btnGuardar" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar" rel="1" >
											Guardar
											</button>
										</div>
									</div>
									<div class="col-6">
										<div class="text-center">
											<button id="btnCancel" name="btnCancel" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditEliminar" rel="1" >
												Eliminar
											</button>
											<input type="hidden" id="rol" name="rol" value="<?php echo $_SESSION['user']['rol']; ?>" />
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>