<?php /* Template Name: Informe Mandamientos Judiciales Rol 1 */ ?>
<?php if (!isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/logout/' ); } ?>
<?php if ($_SESSION['user']['rol'] != 'rol-1') { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php $id = (isset($_GET['id'])) ? (string)trim($_GET['id']) : ''; ?>
<?php if ($id) { $formato = get_post($id); } else { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						<?php echo get_field("titulo", $formato->ID); ?>
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<?php
			//Query News
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'posts_per_page'   => 10,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'dato',
				'post_status'      => 'publish',
				'paged'			   => $paged,
				'suppress_filters' => false,
				'meta_query' => array(
					array(
						'key' => 'formato',
						'value' => $formato->ID,
						'compare' => '='
					)
				)
			);
			$query = new WP_Query( $args );
		?>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-tabla-fiscalia-coahuila">
						<table class="table table-borderless table-striped table-responsive tabla-responsiva">
							<thead>
								<tr>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla">Titulo</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla padLeft75rem">Fecha</th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
									<th scope="col" class="ms-bold font14 uppercase edit-title-tabla"></th>
								</tr>
							</thead>
							<tbody>
								<?php while ( $query->have_posts() ) : $query->the_post(); setup_postdata( $post ); ?>
								<tr>
									<th scope="row" class="ms-regular font14"><?php echo get_field("titulo", $post->ID); ?></th>
									<td class="ms-regular font14"><?php echo get_the_time('j / M / Y', $post->ID); ?></td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/orden/ver-csv/?id=<?php echo $post->ID; ?>" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td><a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/orden/editar-tabla/?id=<?php echo $post->ID; ?>" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>	
								</tr>
								<?php endwhile; wp_reset_postdata(); ?>
								<!--
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/editar-tabla/" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/editar-tabla/" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/editar-tabla/" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/editar-tabla/" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/editar-tabla/" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/editar-tabla/" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/editar-tabla/" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/editar-tabla/" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>
								</tr>
								<tr>
									<th scope="row" class="ms-regular font14">Titulo de la información estadistica</th>
									<td class="ms-regular font14">18 / Sep / 2018</td>
									<td class="p-relative"><div class="linea-ver-tabla"></div><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/ver-csv/" class="float-right ms-bold font11 c-green uppercase padTop5">Ver</a></td>
									<td><a href="<?php bloginfo("url"); ?>/informacion-estadistica/orden/editar-tabla/" class="float-left ms-bold font11 c-green uppercase padTop5">Editar</a></td>
								</tr>
								-->
							</tbody>
						</table>
						<!--
						<div class="col text-center border-line">
							<div class="bloque-tabla">
								<form class="form-inline ms-medium font11">
									<label class="my-1 mr-2" for="listaUsuarios">Articulos por página:</label>
									<select class="custom-select my-1 mr-sm-2 btEditListUser" id="listaUsuarios">
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option selected  value="10">10</option>
									</select>
									<div class="col">
										<span class="">2 - 10 de 30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
										<a href="#"><i class="fas fa-chevron-left c-green font14"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="#"><i class="fas fa-chevron-right font14 c-green"></i></a>
									</div>
								</form>
							</div>
						</div>
						-->
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>