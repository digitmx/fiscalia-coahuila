<?php /* Template Name: Informe Mandamientos Judiciales CSV Editar Rol 2 */ ?>
<?php if (!isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/logout/' ); } ?>
<?php if ($_SESSION['user']['rol'] != 'rol-2') { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php $id = (isset($_GET['id'])) ? (string)trim($_GET['id']) : ''; ?>
<?php if ($id) { $datos = get_post($id); } else { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php $formato = get_post(get_field("formato", $datos->ID)); ?>
<?php $json = get_field("datos", $datos->ID); $json_decode = json_decode($json); ?>
<?php $tabla_header = $json_decode[0]; $contador_json = 0; ?>
<?php get_header(); ?>

	<?php get_template_part("includes/navbar","fiscalia-rol2"); ?>
	
	<div class="container-fluid">
		<div class="row text-center marTop140">
			<div class="col">
				<div class="titulo-detalles">
					<h1 class="ms-light font30 lineFormulario p-relative uppercase">
						<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/orden/?id=<?php echo $formato->ID; ?>" class="back-his"><i class="fas fa-chevron-left c-green font20 back-his-pos"></i></a>
						<?php echo get_field("titulo", $id); ?>
					</h1>
					<div class="linea-titulo"></div>
				</div>
			</div>
		</div>
		<div class="container-fluid marTop30 padBot30">
			<div class="row justify-content-center">
				<div class="col col-lg-10">
					<div class="contenedor-tabla-fiscalia-coahuila-csv">
						<!--
						<table class="table table-responsive">
							<thead>
								<tr>
									<th scope="col" class="ms-semibold font13 uppercase edit-title-tabla-csv">No.</th>
									<th scope="col" class="ms-semibold font13 uppercase edit-title-tabla-csv">Región</th>
									<th scope="col" class="ms-semibold font13 uppercase edit-title-tabla-csv">NÚMERO DE<br>PROCESO O<br>CAUSA PENAL</th>
									<th scope="col" class="ms-semibold font13 uppercase edit-title-tabla-csv">JUZGADO<br>(TRADICIONAL O DEL<br>SISTEMA ACUSATORIO)</th>
									<th scope="col" class="ms-semibold font13 uppercase edit-title-tabla-csv">DATOS DEL INCULPADO O IMPUTADO (EDAD,<br>FECHA DE NAMIENTO, NACIONALIDAD,<br>APODO, MEDIA FILIACION…ETC)</th>
									<th scope="col" class="ms-semibold font13 uppercase edit-title-tabla-csv">DOMICILIO DEL<br>INCULPADO</th>
									<th scope="col" class="ms-semibold font13 uppercase edit-title-tabla-csv">DELITO</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">1</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">2</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">3</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">4</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">5</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">6</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
								<tr>
									<th scope="row" class="ms-bold font12 edit-tabla-csv text-center">7</th>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
									<td class="ms-regular font12 edit-tabla-csv">Edad:24 Años<br>Fecha de nacimiento: 01 / 03 / 1990<br>Nacionalidad: Mexicana<br>Apodo: Conocido</td>
									<td class="ms-regular font12 edit-tabla-csv">Nombre de calle, Col.<br>Nueva, Delegación,<br>Ciudad CP</td>
									<td class="ms-regular font12 edit-tabla-csv">Lorem ipsum dolor</td>
								</tr>
							</tbody>
						</table>
						-->
						<div id="table-edit-csv"></div>
						<?php
							$data = '';
							$contador_field = 0;
							foreach ($json_decode as $row) 
							{ 
								$contador_json++;
								if ($contador_json > 1) 
								{ 
									$data.= '{';
									for ($i = 0; $i < count($row); $i++) 
									{
										$contador_field++;
										$data.= '"'.sanitize_title($tabla_header[$i]).'":"'.$row[$i].'",';
										if (count($row) == $contador_field) { $data.='},'; $contador_field = 0; }
									}
								}
							}
						?>
						<script>
							$( document ).ready(function() {
								//Read Values
								var base_url = $('#base_url').val();
								var theme_url = $('#theme_url').val();
		
								//Data Table
								var tabledata = [
									<?php echo $data; ?>
								 ];
								 
								//Table
								var table = new Tabulator("#table-edit-csv", {
								 	height:400, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
								 	data:tabledata, //assign data to table
								 	layout:"fitColumns", //fit columns to width of table (optional)
								 	pagination:"local",
								 	paginationSize:10,
								 	reactiveData:true, //turn on data reactivity
								 	locale:"es-mx",
								 	langs:{
								        "es-mx":{
								            "custom":{
								                "site_name":"What A Great Table Based Site!!!",
								            },
								            "columns":{
								                "name":"Name", //replace the title of column name with the value "Name";
								            },
								            "pagination":{
								                "first":"Primero", //text for the first page button
								                "first_title":"Primer Página", //tooltip text for the first page button
								                "last":"Último",
								                "last_title":"Última Página",
								                "prev":"Anterior",
								                "prev_title":"Página Anterior",
								                "next":"Siguiente",
								                "next_title":"Página Siguiente",
								            },
								        }
								    },
								 	columns:[ //Define Table Columns
								 		<?php foreach ($tabla_header as $header) { ?>
									 	{title:"<?php echo $header; ?>", field:"<?php echo sanitize_title($header); ?>", editor:"input", headerSort:false, cssClass:"white"},
									 	<?php } ?>
								 	],
								});
								
								//btnAgregarRegistro click
								$('#btAgregarRegistro').on('click', function(e) {
									tabledata.push({
								    	<?php foreach ($tabla_header as $header) { ?>
								    		<?php echo '"' . sanitize_title($header) . '":"",'; ?>
								    	<?php } ?>
								    });
								});
								
								//btnGuardarRegistrosCSV click
								$('#btnGuardarRegistrosCSV').on('click', function(e) {
									e.preventDefault();
									
									var data = table.getData();
									var json = [];
									var json_tmp = [];
									<?php $contador_json = 0; ?>
									<?php foreach ($tabla_header as $header) { ?>
									json_tmp.push("<?php echo $header; ?>");
									<?php } ?>
									json.push(json_tmp);
									$.each( data, function( key, value ) {
										var temp = []
										$.each( value, function( keyB, valueB ) {
											temp.push(valueB);
										});
										json.push(temp);
									});
									json = JSON.stringify(json);
									
									var post_id = $('#post_id').val();
									var formato_id = $('#formato_id').val();
									var rol = $('#rol').val();
									var param = '{"msg": "updateDataForm","fields": {"post_id": "' + post_id + '","datos": ' + json + '}}';
									console.log(param);
									
									//Request API
									$.post(base_url + '/api', { param: param }).done(function( data ) {
										console.log(data);
										
										//Check Result
										if (data.status == 1)
										{
											//Show Success
											window.location.href = base_url + '/' + rol + '/informacion-estadistica/orden/?id=' + formato_id;
							     		}
								 		else
								 		{
								 			//Show Error
								 			alert(data.msg);
							     		}
							    	});	
									
									return false;
								});
							});
						</script>
						<div class="row justify-content-center btnGuardarEditar">
							<div class="col col-lg-10">
								<div class="row justify-content-center">
									<div class="col-4 col-lg-3">
										<div class="text-center">
											<button id="btAgregarRegistro" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" rel="1" >
											Agregar Registro
											</button>
										</div>
									</div>
									<div class="col-6 col-lg-3">
										<div class="text-center">
											<input type="hidden" id="post_id" name="post_id" value="<?php echo $id; ?>" />
											<input type="hidden" id="formato_id" name="formato_id" value="<?php echo $formato->ID; ?>" />
											<input type="hidden" id="rol" name="rol" value="<?php echo $_SESSION['user']['rol']; ?>" />
											<button id="btnGuardarRegistrosCSV" type="submit" class="btn btn-lg btn-block ms-bold font11 btEditGuardar-csv" rel="1" >
												Guardar
											</button>
										</div>
									</div>
									<div class="col-6 col-lg-3">
										<div class="text-center">
											<a href="<?php bloginfo("url"); ?>/<?php echo $_SESSION['user']['rol']; ?>/informacion-estadistica/orden/?id=<?php echo $formato->ID; ?>" class="btn btn-lg btn-block ms-bold font11 btEditEliminar-csv">
												Cancelar
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>