		<nav class="navbar navbar-expand-lg navbar-light fixed-top color-navbarFiscalia padNavbarEdit">
			<a class="navbar-brand editNavbrandFiscalia" href="<?php bloginfo("url"); ?>/rol-4/">
				<img src="<?php bloginfo("template_directory"); ?>/img/logotexto-fiscalia.png" class="img-fluid mx-auto d-block" alt="Responsive image">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class=""><i class="fas fa-align-justify text-white"></i></span>
			</button>
			
			<?php
				$obj_id = get_queried_object_id();
				$current_url = get_permalink( $obj_id );
				$segments = explode('/', $current_url);	
			?>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item <?=(in_array('informacion-estadistica',$segments)) ? 'active ':'';?>navEditlinkSoc">
						<a class="nav-link ms-light font14 text-white" href="<?php bloginfo("url"); ?>/rol-4/informacion-estadistica/">INFORMACIÓN ESTADISTICA<span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item <?=(in_array('notificaciones',$segments)) ? 'active ':'';?>navEditlinkSoc">
						<a class="nav-link ms-light font14 text-white" href="<?php bloginfo("url"); ?>/rol-4/notificaciones/">NOTIFICACIÓNES</a>
					</li>
					<li class="nav-item d-lg-none posRelative">
						<div class="colorBknav"></div>
						<a class="nav-link text-center" href="<?php bloginfo("url"); ?>/rol-4/">
							<div class="btnBacknav">
								<img src="<?php bloginfo("template_directory"); ?>/img/icono--perfilfiscalia.svg" class="img-fluid mx-auto" alt="Responsive image">
								<span class="ms-light font13 text-white navEditlinkSoc">Fiscalía</span>
								<img src="<?php bloginfo("template_directory"); ?>/img/icono--entrar.svg" class="img-fluid mx-auto" alt="Responsive image">
							</div>
						</a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0 posRelative wid370">
					<button id="index" class="btn my-2 my-sm-0 ms-light font13 d-none d-lg-block btnNavFiscalia logOut" type="submit">
						<div class="circulobtn"></div>
						<img src="<?=($_SESSION['user']['avatar']) ? $_SESSION['user']['avatar'] : get_bloginfo("template_directory").'/img/placeholder-avatar.png"'; ?>" class="img-fluid mx-auto borde-usuario avatar-img" alt="Responsive image">
						<span class="ms-light font13 text-white navEditlink"><?= ($_SESSION['user']['puesto']) ? $_SESSION['user']['puesto'] : 'Gestor en Unidades...'; ?></span>
						<img src="<?php bloginfo("template_directory"); ?>/img/icono--entrar.svg" class="img-fluid mx-auto" alt="Responsive image">
					</button>
				</form>
			</div>
		</nav>