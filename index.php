<?php if (isset($_SESSION['logged'])) { wp_redirect( get_bloginfo( 'url' ) . '/'.$_SESSION['user']['rol'].'/' ); } ?>
<?php get_header(); ?>

	<div class="container-fluid bgIndex">
		<div class="container indexSom">
			<div class="row sombraInd">
				<div class="col-8 centro">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<img src="<?php bloginfo("template_directory"); ?>/img/logotexto-fiscalia.png" class="img-fluid mx-auto d-block" alt="Responsive image">
							</div>
						</div>
					</div>
					<div class="container">
						<form id="login_form" name="login_form">
							<div class="row justify-content-center">
								<div class="col-6 col-sm-6 text-white">
									<div class="text-center padTop80 p-relative">
										<input type="text" id="inputEmail" name="inputEmail" placeholder="Correo" class="col">
										<i class="far fa-user icon-img"></i>
									</div>	
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-6 col-sm-6 text-white">
									<div class="text-center padTop40 p-relative">
										<input type="password" id="inputPassword" name="inputPassword" placeholder="Contraseña" class="col">
										<i class="fas fa-lock icon-img"></i>
									</div>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-6 col-sm-6 text-white">
									<div class="text-center padTop40">
										<button id="btnSubmit" name="btnSubmit" type="button" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="userLogin" class="btn ms-bold font14 c-white btnLogin col" rel="1">
										Enviar <img src="<?php bloginfo("template_directory"); ?>/img/icono--entrar.svg" class="img-fluid mx-auto" alt="Responsive image">
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="base_url" name="base_url" value="<?php bloginfo('url'); ?>" />
	<input type="hidden" id="theme_url" name="theme_url" value="<?php bloginfo('template_directory'); ?>" />

<?php //get_footer(); ?>