<!--Footer-->

	<div class="container-fluid footer-line">
		<div class="container">
			<div class="row no-gutters">
				<div class="col-lg-3 col-md-12 col-sm-12 p-relative">
					<div class="right-line-f-img"></div>
					<div class="img-footer">
						<img src="<?php bloginfo("template_directory"); ?>/img/logo-fiscalia.png" class="img-fluid mx-auto d-block" alt="Responsive image">
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-6 p-relative">
					<div class="right-line-f"></div>
					<span class="font12 ms-bold c-green uppercase">Ubicación</span>
					<p class="ms-light font10">
						Humberto Castilla Salas #600, <br>
						Centro Metropolitano, C.P. 25050 <br>
						Saltillo, Coahuila.
					</p>
				</div>
				<div class="col-6 col-md-4 col-lg-3 p-relative">
					<div class="right-line-f-tel"></div>
					<span class="font12 ms-bold c-green uppercase">Teléfono</span>
					<p class="ms-light font10">
						<span class="ms-medium">Conmutador</span><br>
						(844) 438 07 00<br>
						<span class="ms-medium">Unidad de Transparencia:</span><br>
						(844) 438 07 09 ext. 7502
					</p>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-3 texto-centro-mb">
					<span class="font12 ms-bold c-green uppercase">Correo</span>
					<p class="ms-regular font10 no-padding">
						ﬁscaliageneraldelestado@coahuila.gob.mx<br>
						<span class="font12 ms-bold c-green uppercase">Web</span><br>
						www.ﬁscaliageneralcoahuila.gob.mx
					</p>
				</div>
			</div>
		</div>
	</div>

		<input type="hidden" id="base_url" name="base_url" value="<?php bloginfo('url'); ?>" />
		<input type="hidden" id="theme_url" name="theme_url" value="<?php bloginfo('template_directory'); ?>" />
		
		<?php wp_footer(); ?>

	</body>
</html>